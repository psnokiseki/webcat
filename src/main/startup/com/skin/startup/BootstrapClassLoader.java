/*
 * $RCSfile: BootstrapClassLoader.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.startup;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * <p>Title: BootstrapClassLoader</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class BootstrapClassLoader extends URLClassLoader {
    /**
     * @param urls
     */
    public BootstrapClassLoader(URL[] urls) {
        super(urls);
    }

    /**
     * @param urls
     * @param parent
     */
    public BootstrapClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    /**
     * @param parent
     * @param lib
     * @return HotBootClassLoader
     */
    public static BootstrapClassLoader getClassLoader(final ClassLoader parent, final File lib) {
        URL[] urls = getRepositories(lib.listFiles());
        return getClassLoader(parent, urls);
    }

    /**
     * @param parent
     * @param repositories
     * @return HotBootClassLoader
     */
    public static BootstrapClassLoader getClassLoader(final ClassLoader parent, final URL[] repositories) {
        ClassLoader classLoader = AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
            @Override
            public ClassLoader run() {
                if(parent == null) {
                    return new BootstrapClassLoader(repositories);
                }
                else {
                    return new BootstrapClassLoader(repositories, parent);
                }
            }
        });
        return (BootstrapClassLoader)classLoader;
    }

    /**
     * @param parent
     * @param repositories
     * @return ClassLoader
     */
    public static ClassLoader getClassLoader(final ClassLoader parent, final List<URL> repositories) {
        URL[] urls = new URL[repositories.size()];
        repositories.toArray(urls);
        return getClassLoader(parent, urls);
    }

    /**
     * @param files
     * @return URL[]
     */
    public static URL[] getRepositories(File[] files) {
        Set<URL> set = new LinkedHashSet<URL>();

        if(files != null) {
            for(File file : files) {
                String fileName = file.getName().toLowerCase(Locale.ENGLISH);

                if(fileName.endsWith(".jar") || fileName.endsWith(".zip")) {
                    try {
                        set.add(file.toURI().toURL());
                    }
                    catch(IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        URL[] urls = new URL[set.size()];
        set.toArray(urls);
        return urls;
    }

    /**
     * @param urls
     * @param files
     * @return URL[]
     */
    public static List<URL> addRepositories(List<URL> urls, File[] files) {
        Set<URL> set = new LinkedHashSet<URL>();

        if(files != null) {
            for(File file : files) {
                String fileName = file.getName().toLowerCase(Locale.ENGLISH);

                if(fileName.endsWith(".jar") || fileName.endsWith(".zip")) {
                    try {
                        set.add(file.toURI().toURL());
                    }
                    catch(IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        urls.addAll(set);
        return urls;
    }
}
