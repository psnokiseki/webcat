/*
 * $RCSfile: DataImportAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-06-20 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.upload.FileItem;
import com.skin.j2ee.upload.MultipartHttpRequest;
import com.skin.j2ee.util.JsonUtil;
import com.skin.webcat.exchange.DataImport;
import com.skin.webcat.exchange.DefaultHandler;
import com.skin.webcat.util.IO;
import com.skin.webcat.util.Jdbc;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: DataImportAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @version 1.0
 */
public class DataImportAction extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(DataImportAction.class);
    
    /**
     * @throws ServletException
     * @throws IOException
     */
    @com.skin.j2ee.annotation.UrlPattern("/webcat/exchange/index.html")
    public void index() throws ServletException, IOException {
        this.forward("/template/webcat/exchange/index.jsp");
    }

    /**
     * @throws ServletException
     * @throws IOException
     */
    @com.skin.j2ee.annotation.UrlPattern("/webcat/exchange/import.html")
    public void execute() throws ServletException, IOException {
        int maxFileSize = 20 * 1024 * 1024;
        int maxBodySize = 20 * 1024 * 1024;
        String name = null;
        String database = null;
        FileItem uploadFile = null;
        MultipartHttpRequest multipartRequest = null;
        String repository = System.getProperty("java.io.tmpdir");

        Connection connection = null;
        InputStream inputStream = null;
        InputStreamReader reader = null;

        try {
            multipartRequest = MultipartHttpRequest.parse(this.request, maxFileSize, maxBodySize, repository);
            uploadFile = multipartRequest.getFileItem("uploadFile");
            name = multipartRequest.getTrimString("name");
            database = multipartRequest.getTrimString("database");

            if(uploadFile == null || !uploadFile.isFileField()) {
                JsonUtil.error(this.request, this.response, 501, "缺少文件！");
                return;
            }

            inputStream = uploadFile.getInputStream();
            reader = new InputStreamReader(inputStream, "utf-8");

            connection = Webcat.getConnection(name, database);
            DataImport dataImport = new DataImport();
            DefaultHandler defaultHandler = new DefaultHandler(connection);
            dataImport.setProcessHandler(defaultHandler);
            dataImport.execute(reader);
            JsonUtil.success(this.request, this.response, true);
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            JsonUtil.error(this.request, this.response, 500, e.getMessage());
        }
        finally {
            Jdbc.close(connection);
            IO.close(inputStream);

            if(uploadFile != null) {
                uploadFile.delete();
            }
            if(multipartRequest != null) {
                multipartRequest.destroy();
            }
        }
    }
}
