/*
 * $RCSfile: MySqlAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.util.JsonUtil;
import com.skin.webcat.database.mysql.MySql;
import com.skin.webcat.database.mysql.MySqlCharset;
import com.skin.webcat.database.mysql.MySqlCollation;
import com.skin.webcat.util.Jdbc;
import com.skin.webcat.util.Sql;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: MySqlAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class MySqlAction extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(MySqlAction.class);

    /**
     * 返回MySql支持的全部字符集
     * @throws ServletException
     * @throws IOException
     */
    @com.skin.j2ee.annotation.UrlPattern("/webcat/mysql/getCharsetList.html")
    public void getCharsetList() throws ServletException, IOException {
        String name = this.getTrimString("name");
        Connection connection = null;

        try {
            connection = Webcat.getConnection(name);
            List<MySqlCharset> charsetList = MySql.getCharsetList(connection);
            JsonUtil.success(this.request, this.response, charsetList);
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            JsonUtil.error(this.request, this.response, 500, "系统错误，请稍后再试！");
        }
        finally {
            Jdbc.close(connection);
        }
    }

    /**
     * 返回MySql支持的collation
     * @throws ServletException
     * @throws IOException
     */
    @com.skin.j2ee.annotation.UrlPattern("/webcat/mysql/getCollationList.html")
    public void getCollationList() throws ServletException, IOException {
        String name = this.getTrimString("name");
        String charset = Sql.filter(this.getTrimString("charset"));
        Connection connection = null;

        try {
            connection = Webcat.getConnection(name);
            List<MySqlCollation> collationList = MySql.getCollationList(connection, charset);
            JsonUtil.success(this.request, this.response, collationList);
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            JsonUtil.error(this.request, this.response, 500, "系统错误，请稍后再试！");
        }
        finally {
            Jdbc.close(connection);
        }
    }
}
