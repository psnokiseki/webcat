/*
 * $RCSfile: SqlFileAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-04-20 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.util.Path;
import com.skin.webcat.database.Column;
import com.skin.webcat.database.Entry;
import com.skin.webcat.database.Record;
import com.skin.webcat.database.sql.SqlResult;
import com.skin.webcat.database.sql.parser.InsertParser;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: SqlFileAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlDataAction extends BaseAction {
    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/sql/data.html")
    public void list() throws IOException, ServletException {
        String filePath = Path.getStrictPath(this.getParameter("path"));
        File file = Webcat.getFile(this.servletContext, filePath);

        if(file != null) {
            String source = Webcat.getSource(this.servletContext, filePath);
            InsertParser insertParser = new InsertParser(new StringReader(source));
            List<Record> recordList = insertParser.parse();
            SqlResult sqlResult = this.getSqlResult(recordList);
            this.setAttribute("filePath", filePath);
            this.setAttribute("fileName", file.getName());
            this.setAttribute("sqlResult", sqlResult);
        }
        this.forward("/template/webcat/sql/sqlData.jsp");
    }

    /**
     * @param recordList
     * @return SqlResult
     */
    public SqlResult getSqlResult(List<Record> recordList) {
        SqlResult sqlResult = new SqlResult(200, "success");

        if(recordList == null || recordList.isEmpty()) {
            return sqlResult;
        }

        Record record = recordList.get(0);
        List<Column> columns = getColumnList(record);
        List<com.skin.webcat.database.sql.Record> records = this.getRecordList(recordList);
        sqlResult.setTableName(record.getTableName());
        sqlResult.setColumns(columns);
        sqlResult.setRecords(records);
        return sqlResult;
    }

    /**
     * @param record
     * @return List<Column>
     */
    public List<Column> getColumnList(Record record) {
        List<Entry> entryList = record.getColumns();
        List<Column> columnList = new ArrayList<Column>();

        for(Entry entry : entryList) {
            Column column = new Column();
            column.setColumnCode(entry.getName());
            column.setColumnName(entry.getName());
            column.setTypeName(this.getJDBCTypeName(entry.getValue()));
            columnList.add(column);
        }
        return columnList;
    }

    /**
     * @param recordList
     * @return List<com.skin.webcat.database.sql.Record>
     */
    private List<com.skin.webcat.database.sql.Record> getRecordList(List<Record> recordList) {
        List<com.skin.webcat.database.sql.Record> result = new ArrayList<com.skin.webcat.database.sql.Record>();
        
        if(recordList != null && recordList.size() > 0) {
            for(Record record : recordList) {
                com.skin.webcat.database.sql.Record r = new com.skin.webcat.database.sql.Record();
                r.setValues(record.getColumnValueList());
                result.add(r);
            }
        }
        return result;
    }

    /**
     * @param value
     * @return String
     */
    private String getJDBCTypeName(Object value) {
        if(value == null) {
            return null;
        }

        if(value instanceof Integer) {
            return "int";
        }
        else if(value instanceof Long) {
            return "bigint";
        }
        else if(value instanceof Float) {
            return "float";
        }
        else if(value instanceof Double) {
            return "double";
        }
        else if(value instanceof String) {
            return "varchar";
        }
        else if(value instanceof Date) {
            return "timestamp";
        }
        else {
            return value.getClass().getName();
        }
    }
}
