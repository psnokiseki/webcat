/*
 * $RCSfile: BinLogAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;

/**
 * <p>Title: BinLogAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class BinLogAction extends BaseAction {
    /**
     * @throws ServletException
     * @throws IOException
     */
    @com.skin.j2ee.annotation.UrlPattern("/webcat/mysql/binlog.html")
    public void index() throws ServletException, IOException {
    }
}
