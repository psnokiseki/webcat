/*
 * $RCSfile: IndexAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.j2ee.sso.Client;
import com.skin.j2ee.sso.session.UserSession;
import com.skin.j2ee.util.Request;
import com.skin.j2ee.util.UpdateChecker;
import com.skin.util.IP;

/**
 * <p>Title: IndexAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class IndexAction extends BaseAction {
    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/index.html")
    public void execute() throws IOException, ServletException {
        String url = UpdateChecker.getUrl();
        boolean hasNewVersion = UpdateChecker.has();
        String remoteIp = Request.getRemoteAddress(this.request);
        UserSession userSession = Client.getSession(this.request);

        this.setAttribute("localIp", IP.LOCAL);
        this.setAttribute("remoteIp", remoteIp);
        this.setAttribute("appDownloadUrl", url);
        this.setAttribute("hasNewVersion", hasNewVersion);
        this.setAttribute("userSession", userSession);
        this.forward("/template/webcat/index.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/blank.html")
    public void blank() throws IOException, ServletException {
        this.forward("/template/webcat/blank.jsp");
    }
}
