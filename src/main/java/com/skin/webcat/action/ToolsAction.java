/*
 * $RCSfile: ToolsAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.util.IP;

/**
 * <p>Title: ToolsAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ToolsAction extends BaseAction {
    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools.html")
    public void execute() throws IOException, ServletException {
        this.setAttribute("localIp", IP.LOCAL);
        this.forward("/template/tools/index.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/timestamp.html")
    public void timestamp() throws IOException, ServletException {
        this.setAttribute("localIp", IP.LOCAL);
        this.forward("/template/tools/timestamp.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/httpStatus.html")
    public void httpStatus() throws IOException, ServletException {
        this.setAttribute("localIp", IP.LOCAL);
        this.forward("/template/tools/httpStatus.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/htmlFormat.html")
    public void htmlFormat() throws IOException, ServletException {
        this.setAttribute("localIp", IP.LOCAL);
        this.forward("/template/tools/htmlFormat.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/base64.html")
    public void base64() throws IOException, ServletException {
        this.setAttribute("localIp", IP.LOCAL);
        this.forward("/template/tools/base64.jsp");
    }
}
