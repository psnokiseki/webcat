/*
 * $RCSfile: InsertAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-06-20 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action.sql;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.j2ee.util.JsonUtil;
import com.skin.webcat.database.Record;
import com.skin.webcat.database.sql.parser.InsertParser;

/**
 * <p>Title: InsertAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @version 1.0
 */
public class InsertAction extends BaseAction {
    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/sql/insert.html")
    public void insert() throws IOException, ServletException {
        this.forward("/template/webcat/sql/insertEdit.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/sql/insert/parse.html")
    public void parse() throws IOException, ServletException {
        String sql = this.getParameter("sql", "");
        InsertParser insertParser = new InsertParser(new StringReader(sql));
        List<Record> recordList = insertParser.parse();

        if(recordList != null && recordList.size() > 0) {
            Record record = recordList.get(0);
            JsonUtil.success(this.request, this.response, record);
        }
        JsonUtil.success(this.request, this.response, null);
    }
}
