/*
 * $RCSfile: CreateAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-06-20 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action.sql;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.j2ee.util.JsonUtil;
import com.skin.webcat.database.Table;
import com.skin.webcat.database.dialect.Dialect;
import com.skin.webcat.database.dialect.MySQLDialect;
import com.skin.webcat.database.sql.parser.CreateParser;

/**
 * <p>Title: CreateAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @version 1.0
 */
public class CreateAction extends BaseAction {
    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/sql/create.html")
    public void create() throws IOException, ServletException {
        this.forward("/template/webcat/sql/createEdit.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/webcat/tools/sql/create/parse.html")
    public void parse() throws IOException, ServletException {
        String sql = this.getParameter("sql", "");
        Dialect dialect = new MySQLDialect();
        CreateParser createParser = new CreateParser(dialect);
        List<Table> tableList = createParser.parse(sql);

        if(tableList != null && tableList.size() > 0) {
            Table table = tableList.get(0);
            JsonUtil.success(this.request, this.response, table);
        }
        JsonUtil.success(this.request, this.response, null);
    }
}
