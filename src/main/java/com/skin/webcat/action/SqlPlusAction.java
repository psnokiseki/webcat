/*
 * $RCSfile: SqlPlusAction.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-03-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;

import javax.servlet.ServletException;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.webcat.web.WebcatServlet;

/**
 * <p>Title: SqlPlusAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlPlusAction extends BaseAction {
    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/sqlplus.html")
    public void query() throws ServletException, IOException {
        String name = this.getTrimString("name");
        String database = this.getTrimString("database");
        String tableName = this.getTrimString("tableName");
        this.setAttribute("name", name);
        this.setAttribute("database", database);
        this.setAttribute("tableName", tableName);
        this.forward("/template/webcat/sqlplus.jsp");
    }

    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/sqlplus/execute.html")
    public void execute() throws ServletException, IOException {
        String name = this.getTrimString("name");
        String database = this.getTrimString("database");
        String sql = this.getTrimString("sql");
        WebcatServlet.execute(this.request, this.response, name, database, sql);
    }
}
