/*
 * $RCSfile: Webcat.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.util;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.util.Path;
import com.skin.webcat.datasource.ConnectionManager;

/**
 * <p>Title: Webcat</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Webcat {
    private static Logger logger = LoggerFactory.getLogger(Webcat.class);

    /**
     * @return List<String>
     */
    public static List<String> getConnectionList() {
        return ConnectionManager.getConfigNameList();
    }

    /**
     * @param name
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection(String name) throws SQLException {
        return ConnectionManager.getConnection(name);
    }

    /**
     * @param name
     * @param database
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection(String name, String database) throws SQLException {
        return ConnectionManager.getConnection(name, database);
    }

    /**
     * @param work
     * @param filePath
     * @return String
     */
    public static File getFile(File work, String filePath) {
        if(work == null || !work.isDirectory()) {
            return null;
        }

        if(StringUtil.isBlank(filePath)) {
            return work;
        }

        /**
         * File file = new File("/test/mytest", "/");
         * System.out.println("[" + file.getName() + "]");
         * out: []
         */
        String strictPath = Path.getStrictPath(filePath);

        if(strictPath.length() < 1 || strictPath.equals("/")) {
            return work;
        }

        if(Path.contains(work.getAbsolutePath(), filePath)) {
            return new File(work, filePath);
        }
        else {
            return null;
        }
    }

    /**
     * @param servletContext
     * @return String
     */
    public static File getWork(ServletContext servletContext) {
        return new File(servletContext.getRealPath("/WEB-INF/sqls"));
    }

    /**
     * @param servletContext
     * @param filePath
     * @return String
     */
    public static File getFile(ServletContext servletContext, String filePath) {
        File work = getWork(servletContext);
        return Webcat.getFile(work, filePath);
    }

    /**
     * @param servletContext
     * @param filePath
     * @return String
     */
    public static String getSource(ServletContext servletContext, String filePath) {
        File work = getWork(servletContext);
        return Webcat.getSource(work, filePath);
    }

    /**
     * @param work
     * @param filePath
     * @return String
     */
    public static String getSource(File work, String filePath) {
        File file = getFile(work, filePath);

        if(file != null) {
            return getSource(file);
        }
        else {
            return "";
        }
    }

    /**
     * @param file
     * @return String
     */
    public static String getSource(File file) {
        if(file == null) {
            return "";
        }

        String fileName = file.getName().toLowerCase();

        try {
            if(fileName.endsWith(".link.sql")) {
                String content = IO.read(file, "utf-8");
                String[] list = content.split("\\n");
                StringBuilder buffer = new StringBuilder();
    
                for(String line : list) {
                    line = line.trim();
    
                    if(line.startsWith("#")) {
                        continue;
                    }

                    if(line.endsWith(".sql")) {
                        String sql = IO.read(new File(line), "utf-8");
                        buffer.append(sql);
                    }
                }
                return buffer.toString();
            }
            else if(fileName.endsWith(".sql")) {
                return IO.read(file, "utf-8");
            }
            else {
                return "";
            }
        }
        catch(IOException e) {
            logger.warn(e.getMessage(), e);
        }
        return "";
    }
}
