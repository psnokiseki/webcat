/*
 * $RCSfile: ZipUtil.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-04-01 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * <p>Title: ZipUtil</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ZipUtil {
    /**
     * @param source
     * @param target
     */
    public static void compress(String source, String target) {
        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(target);
            compress(new File(source), outputStream);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(outputStream != null) {
                try {
                    outputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param source
     * @param target
     */
    public static void compress(File source, File target) {
        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(target);
            compress(source, outputStream);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(outputStream != null) {
                try {
                    outputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param source
     * @param outputStream
     */
    public static void compress(File source, OutputStream outputStream) {
        ZipOutputStream zipOutputStream = null;

        try {
            File[] files = source.listFiles();
            zipOutputStream = new ZipOutputStream(outputStream);

            for(int i = 0; i < files.length; i++) {
                compress(files[i], zipOutputStream, "");
            }

            zipOutputStream.finish();
            zipOutputStream.flush();
            outputStream.flush();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param file
     * @param zipOutputStream
     * @param basedir
     * @throws IOException
     */
    public static void compress(File file, ZipOutputStream zipOutputStream, String basedir) throws IOException {
        if(file.isDirectory()) {
            compressDirectory(file, zipOutputStream, basedir);
        }
        else {
            compressFile(file, zipOutputStream, basedir);
        }
    }

    /**
     * @param dir
     * @param zipOutputStream
     * @param basedir
     * @throws IOException
     */
    public static void compressDirectory(File dir, ZipOutputStream zipOutputStream, String basedir) throws IOException {
        File[] files = dir.listFiles();

        if(files.length < 1) {
            ZipEntry entry = new ZipEntry(basedir + dir.getName() + "/");
            zipOutputStream.putNextEntry(entry);
            zipOutputStream.closeEntry();
        }
        else {
            for(int i = 0; i < files.length; i++) {
                compress(files[i], zipOutputStream, basedir + dir.getName() + "/");
            }
        }
    }

    /**
     * @param file
     * @param zipOutputStream
     * @param basedir
     */
    public static void compressFile(File file, ZipOutputStream zipOutputStream, String basedir) {
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(file);
            ZipEntry entry = new ZipEntry(basedir + file.getName());
            zipOutputStream.putNextEntry(entry);
            copy(inputStream, zipOutputStream, 4096);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param file
     * @param target
     * @throws IOException
     */
    public static void unzip(File file, File target) throws IOException {
        ZipFile zipFile = null;
        ZipInputStream zipInputStream = null;
        ZipEntry zipEntry = null;

        try {
            zipFile = new ZipFile(file);
            zipInputStream = new ZipInputStream(new FileInputStream(file));

            while((zipEntry = zipInputStream.getNextEntry()) != null) {
                String fileName = zipEntry.getName();
                File temp = new File(target, fileName);

                if(zipEntry.isDirectory()) {
                    temp.mkdirs();
                    continue;
                }

                if(!temp.getParentFile().exists()) {
                    temp.getParentFile().mkdirs();
                }

                InputStream inputStream = null;

                try {
                    inputStream = zipFile.getInputStream(zipEntry);
                    copy(inputStream, temp);

                    /**
                     * setLastModified
                     */
                    long time = zipEntry.getTime();

                    if(time > -1) {
                        temp.setLastModified(time);
                    }
                }
                finally {
                    if(inputStream != null) {
                        inputStream.close();
                    }
                }
            }
        }
        finally {
            if(zipFile != null) {
                try {
                    zipFile.close();
                }
                catch(IOException e) {
                }
            }

            if(zipInputStream != null) {
                try {
                    zipInputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param inputStream
     * @param file
     * @throws IOException
     */
    public static void copy(InputStream inputStream, File file) throws IOException {
        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
            copy(inputStream, outputStream, 4096);
        }
        finally {
            if(outputStream != null) {
                outputStream.close();
            }
        }
    }

    /**
     * @param inputStream
     * @param outputStream
     * @param bufferSize
     * @throws IOException
     */
    private static void copy(InputStream inputStream, OutputStream outputStream, int bufferSize) throws IOException {
        int length = 0;
        byte[] bytes = new byte[Math.max(bufferSize, 4096)];

        while((length = inputStream.read(bytes)) > -1) {
            outputStream.write(bytes, 0, length);
        }
        outputStream.flush();
    }
}
