/*
 * $RCSfile: BackupContext.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-29 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange.backup;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: BackupContext</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class BackupContext {
    private static final Map<String, Progress> context = new HashMap<String, Progress>();

    /**
     * default
     */
    private BackupContext() {
    }

    /**
     * @param token
     * @param progress
     */
    public static void setProgress(String token, Progress progress) {
        context.put(token, progress);
    }

    /**
     * @param token
     * @return Progress
     */
    public static Progress getProgress(String token) {
        return context.get(token);
    }

    /**
     * @param token
     * @return Progress
     */
    public static Progress remove(String token) {
        return context.remove(token);
    }
}
