/*
 * $RCSfile: NumberTransform.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-03-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange.transform;

/**
 * <p>Title: NumberTransform</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class NumberTransform extends AbstractTransform {
    /**
     * @param object
     * @return String
     */
    @Override
    public String toString(Object object) {
        if(object != null) {
            if(object instanceof Boolean) {
                return (((Boolean)object).booleanValue() ? "1" : "0");
            }
            return object.toString();
        }

        return "NULL";
    }
}
