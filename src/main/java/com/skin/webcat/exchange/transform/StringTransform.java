/*
 * $RCSfile: StringTransform.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-03-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange.transform;

/**
 * <p>Title: StringTransform</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class StringTransform extends AbstractTransform {
    /**
     * default
     */
    public StringTransform() {
        super();
    }

    /**
     * @param quote
     */
    public StringTransform(String quote) {
        super(quote, true);
    }

    /**
     * @param quote
     * @param nullable
     */
    public StringTransform(String quote, boolean nullable) {
        super(quote, nullable);
    }

    /**
     * @param object
     * @return String
     */
    @Override
    public String toString(Object object) {
        if(object != null) {
            return this.quote(object.toString());
        }

        if(this.getNullable()) {
            return "NULL";
        }
        else {
            return this.quote("");
        }
    }
}
