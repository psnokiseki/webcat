/*
 * $RCSfile: ChangeTable.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-01 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database;

import com.skin.webcat.util.Sql;

/**
 * <p>Title: ChangeTable</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ChangeTable extends Changeable {
    private ChangeValue tableName;
    private ChangeValue remarks;

    /**
     * apply
     */
    @Override
    public void apply() {
        this.tableName.apply();
        this.remarks.apply();
    }

    /**
     * @return boolean
     */
    @Override
    public boolean modified() {
        return (this.tableName.modified() || this.remarks.modified());
    }

    /**
     * @param tableName
     * @return String
     */
    @Override
    public String getCreateSql(String tableName) {
        return null;
    }

    /**
     * @param tableName
     * @return String
     */
    @Override
    public String getAlterSql(String tableName) {
        if(!this.modified()) {
            return "";
        }

        StringBuilder buffer = new StringBuilder();

        if(this.tableName.modified()) {
            buffer.append("-- rename table\r\n");
            buffer.append("alter table ");
            buffer.append(this.tableName.getOldValue());
            buffer.append(" rename ");
            buffer.append(this.tableName.getNewValue());
            buffer.append(";\r\n");
        }

        if(this.remarks.modified()) {
            buffer.append("-- alter comment\r\n");
            buffer.append("alter table ");
            buffer.append(this.tableName.getNewValue());
            buffer.append(" comment '");
            buffer.append(Sql.escape(this.remarks.getNewValue()));
            buffer.append("';\r\n");
        }
        return buffer.toString();
    }

    /**
     * @return String
     */
    @Override
    public String validate() {
        if(!this.validate(this.tableName)) {
            return "bad tableName name: [" + this.tableName.getOldValue() + "] - " + "[" + this.tableName.getNewValue() + "]";
        }
        return null;
    }

    /**
     * @return the tableName
     */
    public ChangeValue getTableName() {
        return this.tableName;
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(ChangeValue tableName) {
        this.tableName = tableName;
    }

    /**
     * @return the remarks
     */
    public ChangeValue getRemarks() {
        return this.remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(ChangeValue remarks) {
        this.remarks = remarks;
    }
}
