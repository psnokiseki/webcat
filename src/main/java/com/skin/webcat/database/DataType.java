/*
 * $RCSfile: DataType.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-03-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database;

/**
 * <p>Title: DataType</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class DataType {
    /**
     * unknown type
     */
    public static final int UNKNOWN      = 0;

    /**
     * char
     */
    public static final int CHAR         = 1;

    /**
     * boolean
     */
    public static final int BOOLEAN      = 2;

    /**
     * byte
     */
    public static final int BYTE         = 3;

    /**
     * short
     */
    public static final int SHORT        = 4;

    /**
     * int
     */
    public static final int INT          = 5;

    /**
     * float
     */
    public static final int FLOAT        = 6;

    /**
     * double
     */
    public static final int DOUBLE       = 7;

    /**
     * long
     */
    public static final int LONG         = 8;

    /**
     * Character
     */
    public static final int CHAR_WRAP    = 9;

    /**
     * Boolean
     */
    public static final int BOOLEAN_WRAP = 10;

    /**
     * Byte
     */
    public static final int BYTE_WRAP    = 11;

    /**
     * Short
     */
    public static final int SHORT_WRAP   = 12;

    /**
     * Integer
     */
    public static final int INT_WRAP     = 13;

    /**
     * Float
     */
    public static final int FLOAT_WRAP   = 14;

    /**
     * Double
     */
    public static final int DOUBLE_WRAP  = 15;

    /**
     * Long
     */
    public static final int LONG_WRAP    = 16;

    /**
     * String
     */
    public static final int STRING       = 17;

    /**
     * java.sql.Date
     */
    public static final int DATE         = 18;

    /**
     * Timestamp
     */
    public static final int TIMESTAMP    = 19;
}
