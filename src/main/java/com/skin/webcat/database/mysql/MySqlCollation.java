/*
 * $RCSfile: MySqlCollation.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.mysql;

/**
 * <p>Title: MySqlCollation</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class MySqlCollation implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private long id;
    private String collation;
    private String charset;
    private String isDefault;
    private String isCompiled;
    private int sortLen;

    /**
     * @return the id
     */
    public long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the collation
     */
    public String getCollation() {
        return this.collation;
    }

    /**
     * @param collation the collation to set
     */
    public void setCollation(String collation) {
        this.collation = collation;
    }

    /**
     * @return the charset
     */
    public String getCharset() {
        return this.charset;
    }

    /**
     * @param charset the charset to set
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * @return the isDefault
     */
    public String getIsDefault() {
        return this.isDefault;
    }

    /**
     * @param isDefault the isDefault to set
     */
    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * @return the isCompiled
     */
    public String getIsCompiled() {
        return this.isCompiled;
    }

    /**
     * @param isCompiled the isCompiled to set
     */
    public void setIsCompiled(String isCompiled) {
        this.isCompiled = isCompiled;
    }

    /**
     * @return the sortLen
     */
    public int getSortLen() {
        return this.sortLen;
    }

    /**
     * @param sortLen the sortLen to set
     */
    public void setSortLen(int sortLen) {
        this.sortLen = sortLen;
    }
}
