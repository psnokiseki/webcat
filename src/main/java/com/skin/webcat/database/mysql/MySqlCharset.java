/*
 * $RCSfile: MySqlCharset.java,v $
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.mysql;

/**
 * <p>Title: MySqlCharset</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class MySqlCharset implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String charset;
    private String description;
    private String defaultCollation;
    private int maxLen;

    /**
     * @return the charset
     */
    public String getCharset() {
        return this.charset;
    }

    /**
     * @param charset the charset to set
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the defaultCollation
     */
    public String getDefaultCollation() {
        return this.defaultCollation;
    }

    /**
     * @param defaultCollation the defaultCollation to set
     */
    public void setDefaultCollation(String defaultCollation) {
        this.defaultCollation = defaultCollation;
    }

    /**
     * @return the maxLen
     */
    public int getMaxLen() {
        return this.maxLen;
    }

    /**
     * @param maxLen the maxLen to set
     */
    public void setMaxLen(int maxLen) {
        this.maxLen = maxLen;
    }
}
