/*
 * $RCSfile: Oracel11gDialect.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-03-01 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */

package com.skin.webcat.database.dialect;

/**
 * <p>Title: Oracel11gDialect</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Oracel11gDialect extends Oracle9iDialect {
}
