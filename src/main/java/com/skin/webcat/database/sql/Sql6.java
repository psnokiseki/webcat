/*
 * $RCSfile: Sql6.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-02-16 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.sql;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.webcat.util.Jdbc;

/**
 * <p>Title: Sql6</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Sql6 {
    private static final Logger logger = LoggerFactory.getLogger(SqlPlus.class);

    /**
     * @param connection
     * @param sql
     * @return SqlResult
     */
    public static SqlResult execute(Connection connection, String sql) {
        return execute(connection, new StringReader(sql));
    }

    /**
     * @param connection
     * @param reader
     * @return SqlResult
     */
    public static SqlResult execute(Connection connection, Reader reader) {
        int status = 200;
        StringBuilder buffer = new StringBuilder();
        StringBuilder result = new StringBuilder();

        try {
            String sql = null;
            BufferedReader bufferedReader = SqlPlus.getBufferedReader(reader);

            while((sql = SqlPlus.getCommand(bufferedReader, buffer)) != null) {
                int type = SqlPlus.getAction(sql);
                logger.info("type: {}, sql: {}", type, sql);

                if(type == SqlPlus.SELECT || type == SqlPlus.SHOW) {
                    return SqlPlus.select(connection, sql);
                }

                try {
                    long t1 = System.currentTimeMillis();
                    int count = SqlPlus.update(connection, sql);
                    long t2 = System.currentTimeMillis();

                    result.append("[SQL]: ");
                    result.append(sql);
                    result.append("\r\n");
                    result.append("Affected rows: ");
                    result.append(count);
                    result.append("\r\n");
                    result.append("Time: ");
                    result.append(t2 - t1);
                    result.append("ms\r\n\r\n");
                }
                catch(SQLException e) {
                    logger.error(e.getMessage(), e);
                    status = 500;
                    result.append("[SQL]: ");
                    result.append(sql);
                    result.append("\r\n\r\n");
                    result.append(e.getMessage());
                    result.append("\r\n\r\n");
                    Jdbc.rollback(connection);
                    break;
                }
            }
            Jdbc.commit(connection);
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            status = 500;
            result.append("[ERROR]: ");
            result.append(e.getMessage());
            result.append("\r\n\r\n");
            Jdbc.rollback(connection);
        }
        return new SqlResult(status, result.toString());
    }
}
