/*
 * $RCSfile: SqlParser.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-25 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.sql.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.skin.webcat.io.Stream;
import com.skin.webcat.util.Sql;

/**
 * <p>Title: SqlParser</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlParser {
    /**
     * @param stream
     * @return String
     * @throws IOException
     */
    public static String getToken(Stream stream) throws IOException {
        skipComment(stream);
        int c = stream.peek();

        if(c == Stream.EOF) {
            return "";
        }

        StringBuilder buffer = new StringBuilder();

        if(Sql.isSqlIdentifierStart(c)) {
            buffer.append((char)c);
            stream.read();
        }
        else {
            return "";
        }

        while((c = stream.peek()) != Stream.EOF) {
            if(Sql.isSqlIdentifierPart(c)) {
                buffer.append((char)c);
                stream.read();
            }
            else {
                break;
            }
        }
        skipComment(stream);
        return buffer.toString();
    }

    /**
     * @param stream
     * @return String
     * @throws IOException
     */
    public static String getWord(Stream stream) throws IOException {
        skipComment(stream);

        int q = 32;
        int c = stream.peek();
        StringBuilder buffer = new StringBuilder();

        if(c == '`' || c == '\'' || c == '"' || c == '[') {
            q = c;
            stream.read();
        }

        if(q == '[') {
            q = ']';
        }

        while((c = stream.peek()) != -1) {
            if(q == 32 && !Sql.isSqlIdentifierPart(c)) {
                break;
            }
            else if(c == q) {
                break;
            }
            else {
                buffer.append((char)c);
                stream.read();
            }
        }

        c = stream.peek();

        if(q != 32 && c != q) {
            throw new RuntimeException("expect '" + (char)q + "', but found: '" + (char)c + "', ascii: " + c);
        }

        if(c == q) {
            stream.read();
        }
        skipComment(stream);
        return buffer.toString().trim();
    }

    /**
     * @param stream
     * @return List<String>
     * @throws IOException
     */
    public static List<String> getArray(Stream stream) throws IOException {
        skipComment(stream);
        int i = stream.peek();

        if(i != '(') {
            throw new RuntimeException("expect '(', but found: " + i);
        }

        stream.read();
        List<String> list = new ArrayList<String>();

        while(true) {
            String token = getWord(stream);

            if(token.length() < 1) {
                throw new RuntimeException("expect column name, but found: " + stream.getBufferedString());
            }

            list.add(token);

            if(stream.peek() == ')') {
                stream.read();
                break;
            }

            if(stream.peek() == ',') {
                stream.read();
            }
            else {
                throw new RuntimeException("expect ',', but found: " + stream.getBufferedString());
            }
        }
        skipComment(stream);
        return list;
    }

    /**
     * @param text
     * @return String
     */
    public static String unquote(String text) {
        if(text == null) {
            return "";
        }

        String content = text.trim();
        int start = 0;
        int end = content.length();
        char[] quotes = "`\'\"[]".toCharArray();

        for(char quote : quotes) {
            if(start < content.length() && content.charAt(start) == quote) {
                start++;
            }
            if(end > 0 && content.charAt(end - 1) == quote) {
                end--;
            }
        }
        
        if(start > 0 && end < content.length()) {
            return content.substring(start, end);
        }
        else {
            return content;
        }
    }

    /**
     * @param stream
     * @return Integer
     * @throws IOException
     */
    public static Integer getInteger(Stream stream) throws IOException {
        String token = getWord(stream);

        if(token.trim().length() < 1) {
            return null;
        }

        try {
            return Integer.parseInt(token);
        }
        catch(NumberFormatException e) {
        }
        return null;
    }

    /**
     * @param stream
     * @return String
     * @throws IOException
     */
    public static String getString(Stream stream) throws IOException {
        skipComment(stream);

        if(stream.read() != '\'') {
            throw new RuntimeException("expect keyword '\\''!");
        }

        int i = 0;
        StringBuilder buffer = new StringBuilder();

        while((i = stream.read()) != -1) {
            if(i == '\\') {
                unescape(stream, buffer);
            }
            else if(i == '\'') {
                break;
            }
            else {
                buffer.append((char)i);
            }
        }
        return buffer.toString();
    }

    /**
     * @param stream
     * @return Map<String, String>
     * @throws IOException
     */
    public static Map<String, String> getAttributes(Stream stream) throws IOException {
        int i = 0;
        Map<String, String> attributes = new LinkedHashMap<String, String>();
        skipComment(stream);

        while((i = stream.peek()) != Stream.EOF) {
            if(i == ';') {
                break;
            }

            String name = getToken(stream);

            if(name.length() < 1) {
                break;
            }

            /**
             * syntax:
             * default charset=utf-8
             */
            if(name.equalsIgnoreCase("default")) {
                skipComment(stream);
                name = name + " " + getToken(stream);
            }

            /**
             * syntax:
             * comment '表注释'
             */
            i = stream.peek();

            if(i == '=') {
                stream.read();
            }

            skipComment(stream);
            i = stream.peek();

            if(i == '\'') {
                String value = getString(stream);
                attributes.put(name.toUpperCase(), value);
            }
            else {
                String value = getWord(stream);
                attributes.put(name.toUpperCase(), value);
            }
        }
        return attributes;
    }

    /**
     * @param stream
     * @param buffer
     * @throws IOException
     */
    public static void escape(Stream stream, StringBuilder buffer) throws IOException {
        char c = (char)(stream.peek());

        if(c != Stream.EOF) {
            switch(c) {
                case 'n': {
                    buffer.append('\n');
                    stream.read();
                    break;
                }
                case 't': {
                    buffer.append('\t');
                    stream.read();
                    break;
                }
                case 'b': {
                    buffer.append('\b');
                    stream.read();
                    break;
                }
                case 'r': {
                    buffer.append('\r');
                    stream.read();
                    break;
                }
                case 'f': {
                    buffer.append('\f');
                    stream.read();
                    break;
                }
                case '\'': {
                    buffer.append('\'');
                    stream.read();
                    break;
                }
                case '\"': {
                    buffer.append('\"');
                    stream.read();
                    break;
                }
                case '\\': {
                    buffer.append('\\');
                    stream.read();
                    break;
                }
                case 'u': {
                    char[] cbuf = new char[4];
                    int length = stream.read(cbuf, 0, 4);

                    if(length == 4) {
                        String hex = new String(cbuf);

                        try {
                            Integer value = Integer.parseInt(hex, 16);
                            buffer.append((char)(value.intValue()));
                        }
                        catch(NumberFormatException e) {
                        }
                    }
                    else {
                        /**
                         * ignore or:
                         * buffer.append("\\u");
                         * buffer.append(cbuf, 0, length);
                         */
                    }
                    break;
                }
                default: {
                    char[] cbuf = new char[3];
                    int i = stream.read(cbuf, 0, 3);

                    if(i == 3) {
                        String oct = new String(cbuf);

                        try {
                            Integer value = Integer.parseInt(oct, 8);
                            buffer.append((char)(value.intValue()));
                        }
                        catch(NumberFormatException e) {
                        }
                    }
                    else {
                        /**
                         * ignore
                         */
                    }
                    break;
                }
            }
        }
    }

    /**
     * @param stream
     * @param buffer
     * @throws IOException
     */
    public static void unescape(Stream stream, StringBuilder buffer) throws IOException {
        int c = stream.peek();

        if(c < 0) {
            return;
        }

        switch(c) {
            case 'n':{
                buffer.append("\n");
                stream.read();
                break;
            }
            case 't': {
                buffer.append("\t");
                stream.read();
                break;
            }
            case 'b': {
                buffer.append("\b");
                stream.read();
                break;
            }
            case 'r': {
                buffer.append("\r");
                stream.read();
                break;
            }
            case 'f': {
                buffer.append("\f");
                stream.read();
                break;
            }
            case '\'': {
                buffer.append("\'");
                stream.read();
                break;
            }
            case '\"': {
                buffer.append("\"");
                stream.read();
                break;
            }
            case '\\': {
                buffer.append("\\");
                stream.read();
                break;
            }
            case 'u': {
                char[] cbuf = new char[4];
                int length = stream.read(cbuf, 0, 4);

                if(length == 4) {
                    String hex = new String(cbuf);

                    try {
                        int value = Integer.parseInt(hex, 16);
                        buffer.append((char)value);
                    }
                    catch(NumberFormatException e) {
                    }
                }
                else {
                    /**
                     * ignore
                     */
                }
                break;
            }
            default: {
                char[] cbuf = new char[3];
                int length = stream.read(cbuf, 0, 3);

                if(length == 3) {
                    String hex = new String(cbuf);

                    try {
                        int value = Integer.parseInt(hex, 8);
                        buffer.append((char)value);
                    }
                    catch(NumberFormatException e) {
                    }
                }
                else {
                    /**
                     * ignore
                     */
                }
                break;
            }
        }
    }

    /**
     * @param name
     * @return String
     */
    public static String camel(String name) {
        if(null == name || name.trim().length() < 1) {
            return "";
        }

        String[] subs = name.split("_");
        StringBuilder buffer = new StringBuilder();

        if(name.startsWith("_")) {
            buffer.append("_");
        }

        if(subs.length == 1) {
            String s = subs[0];

            if("ID".equals(s)) {
                buffer.append("Id");
            }
            else if(s.toUpperCase().equals(s)) {
                buffer.append(Character.toUpperCase(s.charAt(0)));
                buffer.append(s.substring(1).toLowerCase());
            }
            else {
                buffer.append(Character.toUpperCase(s.charAt(0))).append(s.substring(1));
            }
        }
        else {
            for(String s : subs) {
                if(s.length() > 0) {
                    if("ID".equals(s)) {
                        buffer.append(s);
                    }
                    else if(s.toUpperCase().equals(s)) {
                        buffer.append(Character.toUpperCase(s.charAt(0)));
                        buffer.append(s.substring(1).toLowerCase());
                    }
                    else {
                        buffer.append(Character.toUpperCase(s.charAt(0))).append(s.substring(1));
                    }
                }
            }
        }

        if(name.endsWith("_")) {
            buffer.append("_");
        }
        return buffer.toString();
    }

    /**
     * @param stream
     * @throws IOException
     */
    public static void skipComment(Stream stream) throws IOException {
        int i = 0;
        stream.skipWhitespace();

        while((i = stream.peek()) != -1) {
            if(i == '/' && stream.peek(1) == '*') {
                stream.read();
                stream.read();
                while((i = stream.read()) != Stream.EOF) {
                    if(i == '*' && stream.peek() == '/') {
                        stream.read();
                        break;
                    }
                }
            }
            else if(i == '-' && stream.peek(1) == '-') {
                stream.read();
                stream.read();
                while((i = stream.read()) != Stream.EOF) {
                    if(i == '\n') {
                        break;
                    }
                }
            }
            else if(i > ' ') {
                break;
            }
            else {
                stream.read();
            }
        }
        stream.skipWhitespace();
    }
}
