/*
 * $RCSfile: SqlPlus.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-02-16 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.sql;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.skin.webcat.database.Column;
import com.skin.webcat.datasource.ConnectionManager;
import com.skin.webcat.util.Jdbc;

/**
 * <p>Title: SqlPlus</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlPlus {
    private Map<String, Object> context;
    private PrintWriter out = null;

    /**
     * create...
     */
    public static final int CREATE = 1;

    /**
     * create...
     */
    public static final int INSERT = 2;

    /**
     * update...
     */
    public static final int UPDATE = 3;

    /**
     * delete...
     */
    public static final int DELETE = 4;

    /**
     * select ...
     */
    public static final int SELECT = 5;

    /**
     * alter...
     */
    public static final int ALTER  = 6;

    /**
     * drop...
     */
    public static final int DROP   = 7;

    /**
     * show...
     */
    public static final int SHOW   = 8;

    /**
     * unknown...
     */
    public static final int UNKNOWN = 99;

    /**
     * @param args
     */
    public static void main(String[] args) {
        SqlPlus sqlPlus = new SqlPlus(null, new PrintWriter(System.out, true));

        try {
            sqlPlus.run(new File("src\\test\\resource\\test.sql"), "utf-8");
        }
        catch (Exception e) {
        }
    }

    /**
     * default
     */
    public SqlPlus() {
        this((Map<String, Object>)null, (PrintWriter)null);
    }

    /**
     * @param context
     * @param out
     */
    public SqlPlus(Map<String, Object> context, PrintWriter out) {
        this.context = new HashMap<String, Object>();
        this.out = out;

        if(context != null) {
            this.context.putAll(context);
        }
    }

    /**
     * @param sql
     * @throws Exception
     */
    public void run(String sql) throws Exception {
        run(new StringReader(sql));
    }

    /**
     * @param sql
     * @throws Exception
     */
    public void run(StringBuilder sql) throws Exception {
        run(new StringReader(sql.toString()));
    }

    /**
     * @param file
     * @param charset
     * @throws Exception
     */
    public void run(File file, String charset) throws Exception {
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(file);
            this.run(new InputStreamReader(inputStream, charset));
        }
        finally {
            close(inputStream);
        }
    }

    /**
     * @param inputStream
     * @param charset
     * @throws Exception
     */
    public void run(InputStream inputStream, String charset) throws Exception {
        this.run(new InputStreamReader(inputStream, charset));
    }

    /**
     * @param reader
     * @throws Exception
     */
    public void run(Reader reader) throws Exception {
        String command = null;
        BufferedReader bufferedReader = null;
        StringBuilder buffer = new StringBuilder();
        Connection connection = null;
        this.info("run");

        try {
            bufferedReader = getBufferedReader(reader);

            while((command = getCommand(bufferedReader, buffer)) != null) {
                /**
                 * 获取数据库连接
                 */
                if(command.startsWith("use ")) {
                    this.info(command);
                    close(connection);
                    connection = ConnectionManager.getConnection(command.substring(4));
                    continue;
                }

                if(command.startsWith("using ")) {
                    this.info(command);
                    close(connection);
                    connection = ConnectionManager.getConnection(command.substring(6));
                    continue;
                }

                if(command.startsWith("echo ")) {
                    this.info(command.substring(4));
                    continue;
                }

                /**
                 * 执行其他sql文件
                 * start create.sql;
                 */
                if(command.startsWith("start ")) {
                    /**
                     * 暂不支持
                     * close(connection);
                     * this.run(getReader(line.substring(6, line.length() - 1)));
                     */
                    continue;
                }

                if(command.equals("connect ")) {
                    /**
                     * 暂不支持
                     * close(connection);
                     * connection = ConnectionManager.connect(command.substring(4));
                     */
                    continue;
                }

                if(command.equals("disconnect")) {
                    this.info("disconnect;");
                    close(connection);
                    connection = null;
                    continue;
                }

                if(command.equals("commit")) {
                    this.info("commit;");
                    commit(connection);
                    continue;
                }

                if(command.equals("rollback")) {
                    this.info("rollback;");
                    rollback(connection);
                    continue;
                }

                String sql = replace(command, this.context);
                int action = getAction(sql);

                if(action == SELECT || action == SHOW) {
                    select(connection, sql);
                    continue;
                }

                int rows = update(connection, sql);
                this.info("effect: " + rows + ", " + sql);
            }
        }
        finally {
            close(connection);
        }
    }

    /**
     * @param content
     */
    public void info(String content) {
        if(this.out != null) {
            this.out.println("[sql] " + content);
        }
    }

    /**
     * @return the context
     */
    public Map<String, Object> getContext() {
        return this.context;
    }

    /**
     * @param context the context to set
     */
    public void setContext(Map<String, Object> context) {
        this.context.clear();
        this.context.putAll(context);
    }

    /**
     * @param name
     * @param value
     */
    public void setAttribute(String name, Object value) {
        this.context.put(name, value);
    }

    /**
     * @param connection
     * @param sql
     * @return SqlResult
     * @throws SQLException 
     */
    public static SqlResult select(Connection connection, String sql) throws SQLException {
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);

            ResultSetMetaData medaData = resultSet.getMetaData();
            String tableName = medaData.getTableName(1);
            List<Column> columns = getColumns(medaData);
            List<Record> records = getRecords(resultSet, 200);

            SqlResult sqlResult = SqlResult.success("success");
            sqlResult.setTableName(tableName);
            sqlResult.setColumns(columns);
            sqlResult.setRecords(records);
            return sqlResult;
        }
        finally {
            Jdbc.close(resultSet);
            Jdbc.close(statement);
        }
    }

    /**
     * @param connection
     * @param sql
     * @return int
     * @throws SQLException
     */
    public static int update(Connection connection, String sql) throws SQLException {
        Statement statement = null;

        try {
            statement = connection.createStatement();
            return statement.executeUpdate(sql);
        }
        finally {
            close(statement);
        }
    }

    /**
     * @param medaData
     * @return List<Column>
     * @throws SQLException
     */
    private static List<Column> getColumns(ResultSetMetaData medaData) throws SQLException {
        int columnCount = medaData.getColumnCount();
        List<Column> columns = new ArrayList<Column>();

        for(int i = 1; i <= columnCount; i++) {
            Column column = new Column();
            String columnName = medaData.getColumnName(i);
            int dataType = medaData.getColumnType(i);
            String typeName = medaData.getColumnTypeName(i);
            boolean autoIncrement = medaData.isAutoIncrement(i);

            column.setColumnCode(columnName);
            column.setColumnName(columnName);
            column.setDataType(dataType);
            column.setTypeName(typeName);
            column.setAutoIncrement(autoIncrement);
            columns.add(column);
        }
        return columns;
    }

    /**
     * @param medaData
     * @return List<Column>
     * @throws SQLException
     */
    private static List<Record> getRecords(ResultSet resultSet, int size) throws SQLException {
        int rows = 0;
        List<Record> records = new ArrayList<Record>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while(resultSet.next()) {
            List<Object> values = new ArrayList<Object>();

            for(int i = 1; i <= columnCount; i++) {
                values.add(resultSet.getObject(i));
            }
            records.add(new Record(values));
            rows++;

            if(rows >= size) {
                break;
            }
        }
        return records;
    }

    /**
     * @param bufferedReader
     * @param buffer
     * @return String
     * @throws IOException
     */
    public static String getCommand(BufferedReader bufferedReader, StringBuilder buffer) throws IOException {
        String line = null;
        buffer.setLength(0);

        while((line = bufferedReader.readLine()) != null) {
            line = line.trim();

            if(line.length() < 0 || line.startsWith("--")) {
                continue;
            }

            if(buffer.length() > 0) {
                buffer.append("\r\n");
            }

            if(line.endsWith(";")) {
                buffer.append(line.substring(0, line.length() - 1));
                break;
            }
            else {
                buffer.append(line);
            }
        }

        if(buffer.length() > 0) {
            String command = buffer.toString();
            buffer.setLength(0);
            return command;
        }
        return null;
    }

    /**
     * @param sql
     * @return int
     */
    public static int getAction(String sql) {
        int i = 0;
        int length = sql.length();

        while(i < length && sql.charAt(i) <= ' ') {
            i++;
        }

        int j = i;
        while(j < length && sql.charAt(j) > ' ') {
            j++;
        }

        if(j <= i) {
            return UNKNOWN;
        }

        String action = sql.substring(i, j).toLowerCase();

        if(action.equals("create")) {
            return CREATE;
        }
        else if(action.equals("insert")) {
            return INSERT;
        }
        else if(action.equals("update")) {
            return UPDATE;
        }
        else if(action.equals("delete")) {
            return DELETE;
        }
        else if(action.equals("select")) {
            return SELECT;
        }
        else if(action.equals("alter")) {
            return ALTER;
        }
        else if(action.equals("drop")) {
            return DROP;
        }
        else if(action.equals("show")) {
            return SHOW;
        }
        else {
            return UNKNOWN;
        }
    }

    /**
     * @param text
     * @return boolean
     */
    public static int getEnd(String text) {
        if(text == null) {
            return -1;
        }

        char c;
        for(int i = text.length() - 1; i > -1; i--) {
            c = text.charAt(i);

            if(c > ' ') {
                if(c == ';') {
                    return i;
                }
                else {
                    return -1;
                }
            }
        }
        return -1;
    }

    /**
     * @param reader
     * @return BufferedReader
     */
    public static BufferedReader getBufferedReader(Reader reader) {
        if(reader instanceof BufferedReader) {
            return (BufferedReader)reader;
        }
        return new BufferedReader(reader, 4096);
    }

    /**
     * @param source
     * @param context
     * @return String
     */
    public static String replace(String source, Map<String, Object> context) {
        char c;
        int length = source.length();
        StringBuilder name = new StringBuilder();
        StringBuilder result = new StringBuilder(4096);

        for(int i = 0; i < length; i++) {
            c = source.charAt(i);

            if(c == '$' && i < length - 1 && source.charAt(i + 1) == '{') {
                for(i = i + 2; i < length; i++) {
                    c = source.charAt(i);

                    if(c == '}') {
                        Object value = context.get(name.toString().trim());

                        if(value != null) {
                            result.append(value.toString());
                        }
                        break;
                    }
                    else {
                        name.append(c);
                    }
                }
                name.setLength(0);
            }
            else {
                result.append(c);
            }
        }
        return result.toString();
    }

    /**
     * @param connection
     * @throws SQLException
     */
    public static void commit(Connection connection) throws SQLException {
        if(connection == null) {
            throw new SQLException("connection is null.");
        }
        connection.commit();
    }

    /**
     * @param connection
     * @throws SQLException
     */
    public static void rollback(Connection connection) throws SQLException {
        if(connection == null) {
            throw new SQLException("connection is null.");
        }
        connection.rollback();
    }

    /**
     * @param closeable
     */
    public static void close(Closeable closeable) {
        if(closeable != null) {
            try {
                closeable.close();
            }
            catch (IOException e) {
            }
        }
    }

    /**
     * @param connection
     */
    public static void close(Connection connection) {
        if(connection != null) {
            try {
                if(!connection.isClosed()) {
                    connection.close();
                }
            }
            catch(SQLException e) {
            }
        }
    }

    /**
     * @param statement
     */
    public static void close(Statement statement) {
        if(statement != null) {
            try {
                statement.close();
            }
            catch(SQLException e) {
            }
        }
    }

    /**
     * @param resultSet
     */
    public static void close(ResultSet resultSet) {
        if(resultSet != null) {
            try {
                resultSet.close();
            }
            catch(SQLException e) {
            }
        }
    }
}