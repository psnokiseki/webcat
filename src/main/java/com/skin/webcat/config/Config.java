/*
 * $RCSfile: Config.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-04-10 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.webcat.util.Webcat;

/**
 * <p>Title: Config</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Config {
    private static final Logger logger = LoggerFactory.getLogger(Config.class);
    private static final Properties properties = getProperties("app.properties", "utf-8");

    /**
     * @return String
     */
    public static String getAppName() {
        return properties.getProperty("name");
    }

    /**
     * @param resource
     * @param charset
     * @return Properties
     */
    public static Properties getProperties(String resource, String charset) {
        InputStream inputStream = null;

        try {
            inputStream = Webcat.class.getClassLoader().getResourceAsStream(resource);

            if(inputStream != null) {
                return getProperties(new InputStreamReader(inputStream, charset));
            }
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new Properties();
    }

    /**
     * @param reader
     * @return Map<String, String>
     */
    public static Properties getProperties(Reader reader) {
        Properties properties = new Properties();

        if(reader != null) {
            try {
                String line = null;
                BufferedReader buffer = new BufferedReader(reader);

                while((line = buffer.readLine()) != null) {
                    line = line.trim();

                    if(line.length() < 1) {
                        continue;
                    }

                    if(line.startsWith("#")) {
                        continue;
                    }

                    int i = line.indexOf("=");

                    if(i > -1) {
                        String name = line.substring(0, i).trim();
                        String value = line.substring(i + 1).trim();

                        if(name.length() > 0 && value.length() > 0) {
                            properties.setProperty(name, value);
                        }
                    }
                }
            }
            catch(IOException e) {
            }
        }
        return properties;
    }

    /**
     * @param resource
     * @return InputStream
     */
    public static InputStream getInputStream(String resource) {
        File file = Config.getFile(resource);

        if(file.exists() && file.isFile()) {
            try {
                return new FileInputStream(file);
            }
            catch(FileNotFoundException e) {
            }
        }
        return Config.class.getClassLoader().getResourceAsStream(resource);
    }

    /**
     * @param resource
     * @return File
     */
    public static File getFile(String resource) {
        String appName = Config.getAppName();
        String userHome = System.getProperty("user.home");

        if(resource.startsWith("/") || resource.startsWith("\\")) {
            return new File(userHome, "skinx/" + appName + resource);
        }
        else {
            return new File(userHome, "skinx/" + appName + "/" + resource);
        }
    }
}
