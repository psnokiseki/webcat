/*
 * $RCSfile: Constant.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-1-28 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;

/**
 * <p>Title: Constant</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Constant {
    /**
     * name
     */
    public static final String APP_NAME = "CodeGenerator-1.0.0";

    /**
     * current directory
     */
    public static final String KEY_CURRENT_DIRECTORY = "webcat.file.current.directory";
}
