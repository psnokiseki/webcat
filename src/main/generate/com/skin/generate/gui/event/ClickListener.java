/*
 * $RCSfile: ClickListener.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-01-21 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;

/**
 * <p>Title: ClickListener</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class ClickListener implements ActionListener {
    /**
     * @param event
     */
    @Override
    public final void actionPerformed(final ActionEvent event) {
        final AbstractButton button = this.getSourceButton(event);

        if(button != null) {
            button.setEnabled(false);
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                click(event);

                if(button != null) {
                    button.setEnabled(true);
                }
            }
        });
    }

    /**
     * @param event
     */
    public abstract void click(ActionEvent event);

    /**
     * @return AbstractButton
     */
    private AbstractButton getSourceButton(ActionEvent event) {
        Object source = event.getSource();

        if(source instanceof AbstractButton) {
            return ((AbstractButton)source);
        }
        return null;
    }
}
