/*
 * $RCSfile: EventUtil.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

/**
 * <p>Title: EventUtil</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class EventUtil {
    /**
     * @param button
     * @param actionListener
     */
    public static void click(final JButton button, final ActionListener actionListener) {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        actionListener.actionPerformed(event);
                    }
                });
            }
        });
    }
}
