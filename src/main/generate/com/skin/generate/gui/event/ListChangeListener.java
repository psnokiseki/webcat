/*
 * $RCSfile: ClickListener.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-01-21 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.event;

import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * <p>Title: ClickListener</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class ListChangeListener implements ListDataListener {
    /**
     * @param event
     */
    @Override
    public final void contentsChanged(final ListDataEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                change(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public final void intervalAdded(final ListDataEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                add(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public final void intervalRemoved(final ListDataEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                remove(event);
            }
        });
    }

    /**
     * @param event
     */
    public void add(ListDataEvent event) {
    }

    /**
     * @param event
     */
    public void remove(ListDataEvent event) {
    }

    /**
     * @param event
     */
    public void change(ListDataEvent event) {
    }
}
