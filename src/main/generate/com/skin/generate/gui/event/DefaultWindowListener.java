/*
 * $RCSfile: DefaultWindowListener.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-1-30 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.event;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.SwingUtilities;

/**
 * <p>Title: DefaultWindowListener</p>
 * <p>Description: thread safe listener</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class DefaultWindowListener implements WindowListener {
    /**
     * @param event
     */
    @Override
    public void windowOpened(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                open(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public void windowActivated(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                active(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public void windowDeactivated(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                deactive(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public void windowIconified(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                iconify(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public void windowDeiconified(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                deiconify(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public void windowClosing(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                closing(event);
            }
        });
    }

    /**
     * @param event
     */
    @Override
    public void windowClosed(final WindowEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                close(event);
            }
        });
    }

    /**
     * @param event
     */
    public void open(WindowEvent event) {
    }

    /**
     * @param event
     */
    public void active(WindowEvent event) {
    }

    /**
     * @param event
     */
    public void deactive(WindowEvent event) {
    }

    /**
     * @param event
     */
    public void iconify(WindowEvent event) {
    }

    /**
     * @param event
     */
    public void deiconify(WindowEvent event) {
    }

    /**
     * @param event
     */
    public void closing(WindowEvent event) {
    }

    /**
     * @param event
     */
    public void close(WindowEvent event) {
    }
}
