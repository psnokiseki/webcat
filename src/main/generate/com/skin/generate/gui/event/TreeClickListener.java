/*
 * $RCSfile: ClickListener.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-01-21 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.event;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

/**
 * <p>Title: ClickListener</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class TreeClickListener implements TreeSelectionListener {
    /**
     * @param event
     */
    @Override
    public final void valueChanged(final TreeSelectionEvent event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                click(event);
            }
        });
    }

    /**
     * @param event
     */
    public abstract void click(TreeSelectionEvent event);
}
