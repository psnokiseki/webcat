/*
 * $RCSfile: ProgressDialog.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-01-30 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.skin.generate.gui.event.ClickListener;
import com.skin.generate.gui.event.DefaultWindowListener;

/**
 * <p>Title: ProgressDialog</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ProgressDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private JLabel infoLabel;
    private JProgressBar progressBar;
    private JButton cancelButton;
    private AtomicBoolean running;

    /**
     * @param owner
     * @param title
     * @param modal
     */
    public ProgressDialog(Frame owner, String title, boolean modal)  {
        super(owner, title, modal);
        this.initialize();
    }

    /**
     * @param runnable
     */
    public void open(Runnable runnable) {
        this.running.set(true);

        /**
         * 如果操作时间较长会阻塞UI线程
         * 所以放到单独的线程中执行, runnable中所有操作UI的则需要放到UI线程中执行
         */
        new Thread(runnable).start();
        SwingManager.setCenter(this.getParent(), this);
        this.setVisible(true);
    }

    /**
     * close
     */
    public void close() {
        this.running.set(false);
        this.setVisible(false);
    }

    /**
     * init
     */
    public void initialize() {
        this.running = new AtomicBoolean(false);
        this.setContentPane(getDialogContentPane());
        this.addWindowListener(new DefaultWindowListener() {
            @Override
            public void closing(WindowEvent event) {
                cancel();
            }

            @Override
            public void close(WindowEvent event) {
                cancel();
            }
        });

        this.setSize(600, 160);
        this.setLocation(200, 100);
        this.setResizable(false);
    }

    /**
     * @return JPanel
     */
    private JPanel getDialogContentPane() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanel.add(getProgressBar());
        jPanel.add(getInofLabel());
        jPanel.add(getCancelButton());
        return jPanel;
    }

    /**
     * @return JProgressBar
     */
    private JProgressBar getProgressBar() {
        if(this.progressBar == null) {
            this.progressBar = new JProgressBar(0, 100);
            this.progressBar.setBounds(10, 20, 580, 20);
            this.progressBar.setForeground(Color.GREEN);
            this.progressBar.setMinimum(0);
            this.progressBar.setMaximum(100);
            this.progressBar.setValue(0);
            this.progressBar.setIndeterminate(false);
            this.progressBar.setStringPainted(false);
            this.progressBar.setPreferredSize(new Dimension(580, 20));
            this.progressBar.setBorderPainted(true);
        }
        return this.progressBar;
    }

    /**
     * @return JLabel
     */
    private JLabel getInofLabel() {
        if(this.infoLabel == null) {
            this.infoLabel = new JLabel("");
            this.infoLabel.setBounds(10, 40, 560, 30);
        }
        return this.infoLabel;
    }

    /**
     * @return JButton
     */
    private JButton getCancelButton() {
        if(this.cancelButton == null) {
            this.cancelButton = new JButton("取 消");
            this.cancelButton.setBounds(270, 90, 80, 26);
            this.cancelButton.addActionListener(new ClickListener() {
                @Override
                public void click(ActionEvent event) {
                    cancel();
                } 
            });
        }
        return this.cancelButton;
    }

    /**
     * @return boolean
     */
    public boolean getRunning() {
        return this.running.get();
    }

    /**
     * 
     */
    protected void cancel() {
        this.running.set(false);
    }

    /**
     * reset
     */
    public void reset() {
        this.running.set(false);
        this.setText("");
        this.setString("");
        this.setProgress(0.0f);
    }

    /**
     * @param text
     */
    public void setText(String text) {
        this.infoLabel.setText(text);
    }

    /**
     * @param text
     */
    public void setString(String text) {
        this.progressBar.setString(text);
    }

    /**
     * @param progress
     */
    public void setProgress(float progress) {
        if(progress <= 0.0f) {
            this.progressBar.setValue(0);
            return;
        }

        if(progress >= 1.0f) {
            this.progressBar.setValue(100);
            this.close();
            return;
        }
        this.progressBar.setValue((int)(progress * 100));
    }
}
