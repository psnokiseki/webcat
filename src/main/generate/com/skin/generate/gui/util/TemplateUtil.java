/*
 * $RCSfile: TemplateUtil.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-02-04 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.util;

import java.io.File;
import java.util.List;

import com.skin.generate.Template;
import com.skin.generate.TemplateConfig;
import com.skin.generate.TemplateParser;

/**
 * <p>Title: TemplateUtil</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TemplateUtil {
    /**
     * @param name
     * @return List<Template>
     */
    public static List<Template> getTemplateList(String name) {
        String dir = getDirectory(new String[]{"config", "webapp/config"});
        return TemplateParser.parseTemplates(new File(dir, name));
    }

    /**
     * 使开发环境可测
     * @return List<String>
     */
    public static List<String> getTemplateConfigList() {
        String dir = getDirectory(new String[]{"config", "webapp/config"});
        return TemplateConfig.getTemplateConfigList(dir);
    }

    /**
     * @return String
     */
    public static String getTemplateDirectory() {
        return getDirectory(new String[]{"config/template", "webapp/config/template"});
    }

    /**
     * @param paths
     * @return String
     */
    public static String getDirectory(String[] paths) {
        if(paths != null && paths.length > 0) {
            for(String path : paths) {
                File file = new File(path);
                
                if(file.exists() && file.isDirectory()) {
                    return path;
                }
            }
        }
        return null;
    }
}
