/*
 * $RCSfile: SqlFile.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-02-04 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.util;

import java.io.File;
import java.util.List;

import com.skin.webcat.database.Table;
import com.skin.webcat.database.dialect.Dialect;
import com.skin.webcat.database.dialect.MySQLDialect;
import com.skin.webcat.database.sql.parser.CreateParser;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: SqlFile</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlFile {
    /**
     * @param path
     * @param tableName
     * @return Table
     * @throws Exception
     */
    public static Table getTable(String path, String tableName) throws Exception {
        List<Table> tableList = getTableList(path);
        return getTable(tableList, tableName);
    }

    /**
     * @param path
     * @return List<Table>
     * @throws Exception
     */
    public static List<Table> getTableList(String path) throws Exception {
        File work = new File("sqls");
        File file = Webcat.getFile(work, path);

        if(file != null) {
            String source = Webcat.getSource(work, path);
            Dialect dialect = new MySQLDialect();
            CreateParser sqlParser = new CreateParser(dialect);
            return sqlParser.parse(source);
        }
        return null;
    }

    /**
     * @param tableList
     * @param tableName
     * @return Table
     */
    public static Table getTable(List<Table> tableList, String tableName) {
        if(tableList != null && tableList.size() > 0) {
            for(Table table : tableList) {
                if(tableName.equals(table.getTableName())) {
                    return table;
                }
            }
        }
        return null;
    }

    /**
     * @return String
     */
    public static String getRelativePath() {
        return null;
    }
}
