/*
 * $RCSfile: LoadingDialog.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;
import java.awt.Color;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;


/**
 * <p>Title: LoadingDialog</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class LoadingDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    /**
     * @param owner
     * @param title
     * @param modal
     */
    public LoadingDialog(Frame owner, String title, boolean modal)  {
        super(owner, title, modal);
        this.initialize();
    }

    /**
     * init
     */
    public void initialize() {
        this.setUndecorated(true);
        this.setIconImage(SwingManager.getImage("resource/icon.gif"));
        this.setContentPane(this.getDialogContentPane());
        this.setSize(300, 40);
        this.setResizable(false);
    }

    /**
     * @param runnable
     */
    public void open(final Runnable runnable) {
        if(runnable == null) {
            return;
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    runnable.run();
                }
                finally {
                    close();
                }
            }
        });
        SwingManager.setCenter(this.getParent(), this);
        this.setVisible(true);
    }

    /**
     * close dialog
     */
    public void close() {
        this.setVisible(false);
    }

    /**
     * @return JPanel
     */
    private JPanel getDialogContentPane() {
        JPanel jPanel = new JPanel();
        JLabel jLabel = SwingManager.getJLabel("正在加载，请稍候...");
        jLabel.setBounds(10, 10, 300, 20);
        jPanel.setLayout(null);
        jPanel.setBorder(new LineBorder(new Color(120, 120, 120), 1, true));
        jPanel.setBackground(new Color(255, 255, 255));
        jPanel.add(jLabel);
        return jPanel;
    }
}
