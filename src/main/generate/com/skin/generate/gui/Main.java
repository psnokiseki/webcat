/*
 * $RCSfile: Main.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-01 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.SplashScreen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>Title: Main</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Main extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * @param args
     */
    public static void main(String[] args) {
        new Main().start();
        MainFrame.setLookAndFeel();
        MainFrame frame = new MainFrame();
        frame.open();
    }

    /**
     * run
     */
    @Override
    public void run(){
        try{
            /**
             * java -splash:logo.gif com.skin.generate.gui.Launch
             */
            SplashScreen splash = SplashScreen.getSplashScreen();

            /**
             * 权限不允许使用splash时
             */
            if(splash == null) {
                return;
            }

            splash.setImageURL(SwingManager.getResource("resource/logo.gif"));

            Graphics2D g = splash.createGraphics();
            g.setColor(new Color(254, 153, 15));
            g.setFont(new Font("SansSerif", Font.TRUETYPE_FONT, 10));
            g.drawString("CodeGenerator 1.0.0 xuesong.net", 10, 232);
            splash.update();
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}