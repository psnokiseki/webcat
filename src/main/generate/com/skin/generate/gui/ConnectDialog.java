/*
 * $RCSfile: GeneratorDialog.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListDataEvent;

import com.skin.generate.gui.event.ListChangeListener;
import com.skin.webcat.datasource.ConnectionConfig;

/**
 * <p>Title: GeneratorDialog</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ConnectDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private Color defaultColor = new Color(236, 233, 216);
    private JComboBox jComboBox;

    private JLabel noteLabel;
    private JLabel nameLabel;
    private JLabel urlLabel;
    private JLabel driverLabel;
    private JLabel userNameLabel;
    private JLabel passwordLabel;
    private JLabel catalogLabel;
    private JLabel schemaLabel;
    private JLabel messageLabel;

    private JTextArea urlText;
    private JTextField driverText;
    private JTextField userNameText;
    private JTextField passwordText;
    private JTextField catalogText;
    private JTextField schemaText;

    private JButton ensureButton;
    private JButton cancelButton;
    private ConnectionConfig returnValue;

    /**
     * @param owner
     * @param title
     * @param modal
     */
    public ConnectDialog(Frame owner, String title, boolean modal)  {
        super(owner, title, modal);
        this.initialize();
    }

    /**
     * init
     */
    public void initialize() {
        this.setContentPane(getDialogContentPane());

        this.setSize(600, 480);
        this.setLocation(200, 100);
        this.setResizable(false);
    }

    /**
     * @return JPanel
     */
    private JPanel getDialogContentPane() {
        JPanel jPanel = new JPanel();
        jPanel.add(getImagePanel());
        jPanel.add(getInputPanel());

        jPanel.setLayout(null);
        jPanel.setBounds(0, 0, 600, 480);
        jPanel.setBackground(this.defaultColor);
        return jPanel;
    }

    /**
     * @return JPanel
     */
    private JPanel getImagePanel() {
        JPanel jPanel = new JPanel();
        JLabel jLabel = new JLabel();
        jPanel.setLayout(null);

        jLabel.setIcon(SwingManager.getImageIcon("2.gif"));
        jLabel.setBorder(new EmptyBorder(0, 0, 0, 0));
        jLabel.setBounds(0, 0, 120, 380);
        jPanel.setBounds(20, 30, 120, 380);
        jPanel.add(jLabel);
        return jPanel;
    }

    /**
     * @return JPanel
     */
    private JPanel getInputPanel() {
        this.noteLabel = this.getJLabel("");
        this.nameLabel = this.getJLabel("Name:");
        this.urlLabel = this.getJLabel("Url:");
        this.driverLabel = this.getJLabel("DriverClass:");
        this.userNameLabel = this.getJLabel("UserName:");
        this.passwordLabel = this.getJLabel("Password:");
        this.catalogLabel = this.getJLabel("Catalog:");
        this.schemaLabel = this.getJLabel("Schema:");
        this.messageLabel = this.getJLabel("");

        this.jComboBox = new JComboBox();
        this.urlText = new JTextArea();
        this.driverText = new JTextField();
        this.userNameText = new JTextField();
        this.passwordText = new JTextField();
        this.catalogText = new JTextField();
        this.schemaText = new JTextField();
        this.urlText.setBorder(new LineBorder(new Color(127, 157, 185)));
        this.urlText.setWrapStyleWord(false);
        this.urlText.setLineWrap(true);
        this.jComboBox.setEditable(true);
        this.jComboBox.setMaximumSize(new Dimension(200, 22));

        this.jComboBox.getModel().addListDataListener(new ListChangeListener(){
            @Override
            public void change(ListDataEvent event) {
                ConnectDialog.this.onJComboBoxChange(event);
            }
        });

        final JLabel label = this.messageLabel;
        final JLabel button = this.messageLabel;

        CaretListener caretListener = new CaretListener() {
            @Override
            public void caretUpdate(CaretEvent e) {
                String message = ConnectDialog.this.check();
                label.setText((message != null ? message : ""));
                button.setEnabled((message == null));
            } 
        };

        this.urlText.addCaretListener(caretListener);
        this.driverText.addCaretListener(caretListener);

        int labelWidth = 100;
        int labelHeight = 22;
        int textWidth = 300;
        int textHeight = 22;
        int textXOffset = labelWidth + 4;
        int labelXOffset = 10;
        int yOffset = 30;
        int rHeight = 28;

        this.noteLabel.setBounds(labelXOffset, 0, 320, labelHeight);
        this.noteLabel.setText("请填写连接信息: ");

        this.nameLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.jComboBox.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        this.urlLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.urlText.setBounds(textXOffset, yOffset, textWidth, textHeight + 100);

        yOffset = yOffset + rHeight + 100;
        this.driverLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.driverText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        this.userNameLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.userNameText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        this.passwordLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.passwordText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        this.catalogLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.catalogText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        this.schemaLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.schemaText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        this.messageLabel.setBounds(labelXOffset, yOffset, 400, 40);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBounds(160, 30, 410, 380);
        panel.setBackground(this.defaultColor);

        panel.add(this.noteLabel);
        panel.add(this.nameLabel);
        panel.add(this.jComboBox);

        panel.add(this.urlLabel);
        panel.add(this.urlText);

        panel.add(this.driverLabel);
        panel.add(this.driverText);

        panel.add(this.userNameLabel);
        panel.add(this.userNameText);

        panel.add(this.passwordLabel);
        panel.add(this.passwordText);

        panel.add(this.catalogLabel);
        panel.add(this.catalogText);

        panel.add(this.schemaLabel);
        panel.add(this.schemaText);
        panel.add(this.messageLabel);

        this.ensureButton = new JButton("确 定");
        this.ensureButton.setFont(SwingManager.getDefaultFont());
        this.ensureButton.setBounds(120, 340, 80, 28);
        this.ensureButton.setMargin(new Insets(0, 10, 0, 10));

        this.cancelButton = new JButton("取 消");
        this.cancelButton.setFont(SwingManager.getDefaultFont());
        this.cancelButton.setBounds(240, 340, 80, 28);
        this.cancelButton.setMargin(new Insets(0, 10, 0, 10));
        
        this.ensureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                onEnsureButtonClick(actionEvent);
            }
        });

        this.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                onCancelButtonClick(actionEvent);
            }
        });

        panel.add(this.ensureButton);
        panel.add(this.cancelButton);
        return panel;
    }

    /**
     * @param text
     * @return JLabel
     */
    public JLabel getJLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(SwingManager.getDefaultFont());
        return label;
    }

    /**
     * @param event
     */
    public void onJComboBoxChange(ListDataEvent event) {
        ConnectionConfig config = this.getSelectedItem();

        if(config != null) {
            this.urlText.setText((config.getUrl() != null ? config.getUrl() : ""));
            this.driverText.setText((config.getDriverClass() != null ? config.getDriverClass() : ""));
            this.userNameText.setText((config.getUserName() != null ? config.getUserName() : ""));
            this.passwordText.setText((config.getPassword() != null ? config.getPassword() : ""));
            this.catalogText.setText((config.getCatalog() != null ? config.getCatalog() : ""));
            this.schemaText.setText((config.getSchema() != null ? config.getSchema() : ""));
        }
        else {
            this.urlText.setText("");
            this.driverText.setText("");
            this.userNameText.setText("");
            this.passwordText.setText("");
            this.catalogText.setText("");
            this.schemaText.setText("");
        }

        this.urlText.select(0, 0);
        this.driverText.select(0, 0);

        String message = this.check();
        this.messageLabel.setText((message != null ? message : ""));
        this.ensureButton.setEnabled((message == null));
    }

    /**
     * @param actionEvent
     */
    public void onEnsureButtonClick(ActionEvent actionEvent) {
        ConnectionConfig config = this.getSelectedItem();

        if(config != null) {
            String url = this.urlText.getText();
            String driver = this.driverText.getText();
            String userName = this.userNameText.getText();
            String password = this.passwordText.getText();
            String catalog = this.catalogText.getText();
            String schema = this.schemaText.getText();

            config.setUrl(url);
            config.setDriverClass(driver);
            config.setUserName(userName);
            config.setPassword(password);
            config.setCatalog(catalog);
            config.setSchema(schema);

            this.jComboBox.removeItem(config);
            this.jComboBox.addItem(config);
        }
        this.returnValue = config;
        this.setVisible(false);
    }

    /**
     * @param actionEvent
     */
    public void onCancelButtonClick(ActionEvent actionEvent) {
        this.returnValue = null;
        this.setVisible(false);
    }

    /**
     * @return String
     */
    public String check() {
        String name = this.getConfigName();
        String url = this.urlText.getText();
        String driverClass = this.driverText.getText();

        if(name == null || name.trim().length() < 1) {
            return "Name must be not empty!";
        }

        if(url == null || url.trim().length() < 1) {
            return "Url must be not empty!";
        }

        if(driverClass == null || driverClass.trim().length() < 1) {
            return "DriverClass must be not empty!";
        }
        return null;
    }

    private String getConfigName() {
        ConnectionConfig config = this.getSelectedItem();

        if(config != null) {
            return config.getName();
        }
        return null;
    }

    /**
     * @return ConnectionConfig
     */
    public ConnectionConfig getSelectedItem() {
        return (ConnectionConfig)(this.jComboBox.getSelectedItem());
    }

    /**
     * @return ConnectionConfig
     */
    public ConnectionConfig openDialog() {
        return this.openDialog(null);
    }

    /**
     * @param configs
     * @return ConnectionConfig
     */
    public ConnectionConfig openDialog(List<ConnectionConfig> configs) {
        if(configs != null) {
            this.jComboBox.removeAllItems();

            for(ConnectionConfig config : configs) {
                this.jComboBox.addItem(config);
            }
        }
        this.setVisible(true);
        return this.returnValue;
    }

    /**
     * <p>Title: ListItem</p>
     * <p>Description: </p>
     * <p>Copyright: Copyright (c) 2006</p>
     * @author xuesong.net
     * @version 1.0
     */
    public static class ListItem {
        ConnectionConfig connectionConfig;

        /**
         * @param config
         */
        public ListItem(ConnectionConfig config) {
            this.connectionConfig = config;
        }

        /**
         * @return ConnectionConfig
         */
        public ConnectionConfig getConnectionConfig() {
            return this.connectionConfig;
        }

        /**
         * @return String
         */
        @Override
        public String toString() {
            return this.connectionConfig.getName();
        }
    }
}
