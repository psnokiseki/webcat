/*
 * $RCSfile: GeneratorDialog.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.skin.generate.gui.event.ClickListener;
import com.skin.generate.gui.util.TemplateUtil;

/**
 * <p>Title: GeneratorDialog</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class GeneratorDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    /**
     * 确定
     */
    public static final int ENSURE = 1;

    /**
     * 取消
     */
    public static final int CANCEL = 0;

    private JTextField tableText;
    private JTextField classText;
    private JTextField remarksText;
    private JComboBox encodingComboBox;
    private JCheckBox wrapperCheckBox;
    private JTextField authorText;
    private JComboBox templateComboBox;
    private JTextField outputText;

    private JButton ensureButton;
    private JButton cancelButton;
    private int returnValue = CANCEL;
    private static final List<Charset> charsetList = getFriendlyCharsetList();

    /**
     * @param owner
     * @param title
     * @param modal
     */
    public GeneratorDialog(Frame owner, String title, boolean modal)  {
        super(owner, title, modal);
        this.initialize();
    }

    /**
     * @return int
     */
    public int open() {
        this.templateComboBox.removeAllItems();
        List<String> templates = TemplateUtil.getTemplateConfigList();

        for(String template : templates) {
            this.templateComboBox.addItem(template);
        }

        this.wrapperCheckBox.setSelected(true);
        SwingManager.setCenter(this.getParent(), this);
        this.setVisible(true);
        return this.returnValue;
    }

    /**
     * init
     */
    public void initialize() {
        this.setIconImage(SwingManager.getImage("resource/icon.gif"));
        this.setContentPane(getDialogContentPane());
        this.setSize(600, 480);
        this.setLocation(200, 100);
        this.setResizable(false);
    }

    /**
     * @return JPanel
     */
    private JPanel getDialogContentPane() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanel.add(getImagePanel());
        jPanel.add(getInputPanel());
        jPanel.setBounds(0, 0, 600, 480);
        jPanel.setBackground(SwingManager.getDefaultColor());
        return jPanel;
    }

    private JPanel getImagePanel() {
        JPanel jPanel = new JPanel();
        JLabel jLabel = new JLabel();
        jPanel.setLayout(null);
        jLabel.setIcon(SwingManager.getImageIcon("resource/1.gif"));

        jLabel.setBorder(new EmptyBorder(0, 0, 0, 0));
        jLabel.setBounds(0, 0, 180, 380);
        jPanel.setBounds(20, 30, 180, 380);
        jPanel.add(jLabel);
        return jPanel;
    }

    private JPanel getInputPanel() {
        JLabel noteLabel = SwingManager.getJLabel("");
        JLabel tableLabel = SwingManager.getJLabel("TableName:");
        JLabel classLabel = SwingManager.getJLabel("ClassName:");
        JLabel remarksLabel = SwingManager.getJLabel("Remarks:");
        JLabel encodingLabel = SwingManager.getJLabel("Encoding:");
        JLabel wrapperLabel = SwingManager.getJLabel("Wrapper:");
        JLabel authorLabel = SwingManager.getJLabel("Author:");
        JLabel templateLabel = SwingManager.getJLabel("Template:");
        JLabel outputLabel = SwingManager.getJLabel("Output:");

        this.tableText = new JTextField();
        this.classText = new JTextField();
        this.remarksText = new JTextField();
        this.encodingComboBox = new JComboBox();
        this.wrapperCheckBox = new JCheckBox();
        this.authorText = new JTextField();
        this.templateComboBox = new JComboBox();
        this.outputText = new JTextField("gen/");

        int labelWidth = 80;
        int labelHeight = 22;
        int textWidth = 240;
        int textHeight = 22;
        int textXOffset = labelWidth + 4;
        int labelXOffset = 10;
        int yOffset = 40;
        int rHeight = 28;

        noteLabel.setBounds(labelXOffset, 10, 320, labelHeight);
        noteLabel.setText("生成器选项: ");

        tableLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.tableText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        classLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.classText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        remarksLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.remarksText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        wrapperLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.wrapperCheckBox.setBounds(textXOffset, yOffset, 20, 20);

        yOffset = yOffset + rHeight;
        encodingLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.encodingComboBox.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        authorLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.authorText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        yOffset = yOffset + rHeight;
        templateLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.templateComboBox.setBounds(textXOffset, yOffset, textWidth, textHeight);
        this.templateComboBox.setMaximumSize(new Dimension(200, 22));

        yOffset = yOffset + rHeight;
        outputLabel.setBounds(labelXOffset, yOffset, labelWidth, labelHeight);
        this.outputText.setBounds(textXOffset, yOffset, textWidth, textHeight);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBounds(220, 30, 350, 380);
        panel.setBackground(SwingManager.getDefaultColor());

        panel.add(noteLabel);
        panel.add(tableLabel);
        panel.add(this.tableText);

        panel.add(classLabel);
        panel.add(this.classText);

        panel.add(remarksLabel);
        panel.add(this.remarksText);

        panel.add(encodingLabel);
        panel.add(this.encodingComboBox);

        panel.add(wrapperLabel);
        panel.add(this.wrapperCheckBox);

        panel.add(authorLabel);
        panel.add(this.authorText);

        panel.add(templateLabel);
        panel.add(this.templateComboBox);

        panel.add(outputLabel);
        panel.add(this.outputText);

        this.ensureButton = new JButton("确 定");
        this.ensureButton.setFont(SwingManager.getDefaultFont());
        this.ensureButton.setBounds(120, 320, 80, 28);
        this.ensureButton.setMargin(new Insets(0, 10, 0, 10));

        this.cancelButton = new JButton("取 消");
        this.cancelButton.setFont(SwingManager.getDefaultFont());
        this.cancelButton.setBounds(240, 320, 80, 28);
        this.cancelButton.setMargin(new Insets(0, 10, 0, 10));

        this.ensureButton.addActionListener(new ClickListener() {
            @Override
            public void click(ActionEvent actionEvent) {
                onEnsureButtonClick(actionEvent);
            }
        });

        this.cancelButton.addActionListener(new ClickListener(){
            @Override
            public void click(ActionEvent actionEvent) {
                onCancelButtonClick(actionEvent);
            }
        });

        panel.add(this.ensureButton);
        panel.add(this.cancelButton);

        for(Charset charset : charsetList) {
            this.encodingComboBox.addItem(charset.name());
        }
        return panel;
    }

    /**
     * @param actionEvent
     */
    public void onEnsureButtonClick(ActionEvent actionEvent) {
        this.returnValue = ENSURE;
        this.setVisible(false);
    }

    /**
     * @param actionEvent
     */
    public void onCancelButtonClick(ActionEvent actionEvent) {
        this.returnValue = CANCEL;
        this.setVisible(false);
    }

    /**
     * @return List<Charset>
     */
    private static List<Charset> getFriendlyCharsetList() {
        List<Charset> charsetList = new ArrayList<Charset>();
        TreeMap<String, Charset> charsetMap = new TreeMap<String, Charset>();
        charsetMap.putAll(Charset.availableCharsets());

        String[] charsetNames = new String[]{
                "utf-8", "gbk", "gb2312", "gb18030", "big5"
        }; 

        for(String name : charsetNames) {
            try {
                if(Charset.isSupported(name)) {
                    Charset charset = Charset.forName(name);
                    charsetList.add(charset);
                    charsetMap.remove(charset.name());
                }
            }
            catch(Exception e) {
            }
        }
        charsetList.addAll(charsetMap.values());
        return charsetList;
    }

    /**
     * @param batch
     */
    public void setBatchMode(boolean batch) {
        if(batch) {
            this.tableText.setText("---");
            this.classText.setText("---");
            this.remarksText.setText("---");
            this.tableText.setEditable(false);
            this.classText.setEditable(false);
            this.remarksText.setEditable(false);
        }
        else {
            this.tableText.setEditable(true);
            this.classText.setEditable(true);
            this.remarksText.setEditable(true);
        }
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName) {
        this.tableText.setText(tableName);
    }

    /**
     * @return the tableName
     */
    public String getTableName() {
        return this.tableText.getText();
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.classText.setText(className);
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return this.classText.getText();
    }

    /**
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarksText.setText(remarks);
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return this.remarksText.getText();
    }

    /**
     * @param encoding the encoding to set
     */
    public void setEncoding(String encoding) {
        this.encodingComboBox.setSelectedItem(encoding);
    }

    /**
     * @return the encoding
     */
    public String getEncoding() {
        return (String)(this.encodingComboBox.getSelectedItem());
    }

    /**
     * @param wrapper the wrapper to set
     */
    public void setWrapper(boolean wrapper) {
        this.wrapperCheckBox.setSelected(wrapper);
    }

    /**
     * @return the wrapper
     */
    public boolean getWrapper() {
        return this.wrapperCheckBox.isSelected();
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return this.authorText.getText();
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.authorText.setText(author);
    }

    /**
     * @param template
     */
    public void setTemplate(String template) {
        this.templateComboBox.setSelectedItem(template);
    }

    /**
     * @return String
     */
    public String getTemplate() {
        return (String)(this.templateComboBox.getSelectedItem());
    }

    /**
     * @return the outputPath
     */
    public String getOutputPath() {
        return this.outputText.getText();
    }

    /**
     * @param outputPath the outputPath to set
     */
    public void setOutputPath(String outputPath) {
        this.outputText.setText(outputPath);
    }
}
