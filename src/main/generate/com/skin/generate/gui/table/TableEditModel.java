/*
 * $RCSfile: TableEditModel.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-03-01 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.table;

import javax.swing.table.AbstractTableModel;

/**
 * <p>Title: TableEditModel</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TableEditModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private Class<?>[] columnClass = new Class<?>[]{Boolean.class, String.class, String.class, String.class, String.class, Integer.class, Integer.class, Integer.class, Boolean.class};
    private String[] columnNames = new String[]{"Check", "Name", "FieldName", "TypeName", "JavaType", "ColumnSize", "DecimalDigits", "Precision", "Nullable"};
    private Object[][] data = new Object[][]{};

    /**
     * @return int
     */
    @Override
    public int getColumnCount()  {
        return this.columnNames.length;
    }

    /**
     * @return int
     */
    @Override
    public int getRowCount()  {
        return this.data.length;
    }

    /**
     * @param index
     * @return String
     */
    @Override
    public String getColumnName(int index)  {
        return this.columnNames[index];
    }

    /**
     * @param row
     * @param col
     * @return Object
     */
    @Override
    public Object getValueAt(int row, int col)  {
        return this.data[row][col];
    }

    /**
     * @param c
     * @return Class<?>
     */
    @Override
    public Class<?> getColumnClass(int c)  {
        return this.columnClass[c];
    }

    /**
     * @return int
     */
    @Override
    public boolean isCellEditable(int row, int col)  {
        return (col == 0 || col == 2 || col == 4 || col == 8);
    }

    /**
     * @param value
     * @param row
     * @param col
     */
    @Override
    public void setValueAt(Object value, int row, int col)  {
        if(row < this.data.length && col < this.data[row].length) {
            this.data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }

    /**
     * @param data
     */
    public void setData(Object[][] data) {
        this.data = data;
    }

    /**
     * @return Object[][]
     */
    public Object[][] getData() {
        return this.data;
    }
}
