/*
 * $RCSfile: SwingManager.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import com.skin.webcat.util.IO;

/**
 * <p>Title: SwingManager</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SwingManager {
    private static final Map<String, Image> imageCache = new HashMap<String, Image>();
    private static final Map<String, ImageIcon> iconCache = new HashMap<String, ImageIcon>();
    private static final Font defaultFont = new Font("宋体", Font.TRUETYPE_FONT, 12);
    private static final Color defaultColor = new Color(236, 233, 216);

    /**
     * @param text
     * @return JLabel
     */
    public static JLabel getJLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(SwingManager.getDefaultFont());
        return label;
    }

    /**
     * @return Font
     */
    public static Font getDefaultFont() {
        return defaultFont;
    }

    /**
     * @return Font
     */
    public static Color getDefaultColor() {
        return defaultColor;
    }

    /**
     * @param resource
     * @return Image
     */
    public static Image getImage(String resource) {
        Image image = imageCache.get(resource);

        if(image == null) {
            InputStream inputStream = null;

            try {
                inputStream = SwingManager.class.getResourceAsStream(resource);

                if(inputStream != null) {
                    image = getImage(inputStream);
                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            finally {
                IO.close(inputStream);
            }

            if(image != null) {
                imageCache.put(resource, image);
            }
        }
        return image;
    }

    /**
     * @param resource
     * @return ImageIcon
     */
    public static ImageIcon getImageIcon(String resource) {
        ImageIcon icon = iconCache.get(resource);

        if(icon == null) {
            InputStream inputStream = null;

            try {
                inputStream = SwingManager.class.getResourceAsStream(resource);

                if(inputStream != null) {
                    icon = getImageIcon(inputStream);
                }
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            finally {
                IO.close(inputStream);
            }

            if(icon != null) {
                iconCache.put(resource, icon);
            }
        }
        return icon;
    }

    /**
     * @param inputStream
     * @return Image
     */
    public static Image getImage(InputStream inputStream) {
        try {
            return ImageIO.read(inputStream);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param inputStream
     * @return ImageIcon
     */
    public static ImageIcon getImageIcon(InputStream inputStream) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            IO.copy(inputStream, bos);

            byte[] bytes = bos.toByteArray();
            ImageIcon icon = new ImageIcon(bytes);
            return icon;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param name
     * @return URL
     */
    public static URL getResource(String name) {
        return SwingManager.class.getResource(name);
    }

    /**
     * @param tree
     * @param node
     */
    public static void expand(JTree tree, TreeNode node) {
        List<TreeNode> nodeList = new ArrayList<TreeNode>();
        nodeList.add(node);
        TreeNode current = node;

        while(current.getParent() != null) {
            current = current.getParent();
            nodeList.add(current);
        }

        int size = nodeList.size();
        Object[] path = new Object[nodeList.size()];

        for(int i = 0; i < size; i++) {
            path[i] = nodeList.get(size - i - 1);
        }

        TreePath treePath = new TreePath(path);
        tree.expandPath(treePath);
    }

    /**
     * @param parent
     * @param child
     */
    public static void setCenter(Component parent, Component child) {
        Point point = parent.getLocation();
        double parentWidth = parent.getWidth();
        double parentHeight = parent.getHeight();
        double width = child.getWidth();
        double height = child.getHeight();
        int x = point.x + (int)((parentWidth - width) / 2);
        int y = point.y + (int)((parentHeight - height) / 2);
        child.setLocation(x, y);
    }

    /**
     * @param component
     */
    public static void setCenter(Component component) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        double screenWidth = dimension.getWidth();
        double screenHeight = dimension.getHeight();
        double width = component.getWidth();
        double height = component.getHeight();
        int x = (int)((screenWidth - width) / 2);
        int y = (int)((screenHeight - height) / 2);
        component.setLocation(x, y);
    }
}
