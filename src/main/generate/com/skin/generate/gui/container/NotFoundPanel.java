/*
 * $RCSfile: NotFoundPanel.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-01-29 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.container;

import java.awt.Color;
import java.awt.Font;
import java.util.Map;

import javax.swing.JLabel;

/**
 * <p>Title: NotFoundPanel</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class NotFoundPanel extends BasePanel {
    private static final long serialVersionUID = 1L;
    private JLabel label;

    /**
     * @param frame
     */
    public NotFoundPanel(BaseFrame frame) {
        super(frame);
        this.label = new JLabel("Not Found.");
        this.label.setFont(new Font("SansSerif", Font.TRUETYPE_FONT, 24));
        this.add(this.label);
        this.setBackground(Color.WHITE);
    }

    /**
     * @param params
     */
    @Override
    public void load(Map<String, Object> params) {
        this.setVisible(true);
    }
}
