/*
 * $RCSfile: BaseFrame.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-01-29 $
 *
 * Copyright (C) 2005 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.container;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.generate.gui.LocalStorage;

/**
 * <p>Title: BaseFrame</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class BaseFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private BasePanel blank;
    private BasePanel notfound;
    private Map<String, Action> actions;
    private static final Logger logger = LoggerFactory.getLogger(BaseFrame.class);

    /**
     * default
     */
    public BaseFrame() {
        this.actions = new HashMap<String, Action>();
    }

    /**
     * @param action
     * @param className
     */
    public void registe(String action, String className) {
        Action model = this.actions.get(action);

        if(model != null) {
            model.setName(action);
            model.setClassName(className);
        }
        else {
            this.actions.put(action, new Action(action, className));
        }
    }

    /**
     * @param action
     * @param panel
     */
    public void registe(String action, BasePanel panel) {
        Action model = this.actions.get(action);

        if(model != null) {
            model.setName(action);
            model.setClassName(panel.getClass().getName());
        }
        else {
            this.actions.put(action, new Action(action, panel.getClass().getName()));
        }
        PanelManager.setPanel(panel);
    }

    /**
     * @param action
     * @param params
     */
    public void dispatch(String action, Map<String, Object> params) {
        BasePanel panel = this.getPanel(action);
        String threadName = Thread.currentThread().getName();
        logger.info("{} - action: {}, panel: {}", threadName, action, panel.getClass().getName());

        panel.open(params);
        this.display(panel, params);
    }

    /**
     * @param panel
     * @param params
     */
    public abstract void display(BasePanel panel, Map<String, Object> params);

    /**
     * @param action
     * @return JPanel
     */
    public BasePanel getPanel(String action) {
        Action model = this.actions.get(action);

        if(model == null) {
            return this.getNotFoundPanel();
        }

        BasePanel panel = PanelManager.getPanel(this, model.getClassName(), true);

        if(panel == null) {
            return this.getNotFoundPanel();
        }
        return panel;
    }

    /**
     * @return BasePanel
     */
    public BasePanel getBlankPanel() {
        if(this.blank == null) {
            this.blank = new BlankPanel(this);
            this.blank.setBackground(Color.WHITE);
        }
        return this.blank;
    }

    /**
     * @return BasePanel
     */
    public BasePanel getNotFoundPanel() {
        if(this.notfound == null) {
            this.notfound = new NotFoundPanel(this);
            this.notfound.setBackground(Color.WHITE);
        }
        return this.notfound;
    }

    /**
     * @return LocalStorage
     */
    public abstract LocalStorage getLocalStorage();

    /**
     * @param localStorage
     */
    public abstract void save(LocalStorage localStorage);

    /**
     * @param message
     */
    public void message(String message) {
        javax.swing.JOptionPane.showMessageDialog(this, message, "Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * @param message
     * @return boolean
     */
    public boolean confirm(String message) {
        int returnValue = javax.swing.JOptionPane.showConfirmDialog(this, message, "Confirm", javax.swing.JOptionPane.YES_NO_OPTION);
        return (returnValue == javax.swing.JOptionPane.YES_OPTION);
    }

    /**
     * @param message
     */
    public void error(String message) {
        javax.swing.JOptionPane.showMessageDialog(this, message, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    /**
     * @param t
     */
    public void error(Throwable t) {
        javax.swing.JOptionPane.showMessageDialog(this, t.getMessage(), "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
}
