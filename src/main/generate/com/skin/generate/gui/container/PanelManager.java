/*
 * $RCSfile: PanelManager.java,v $$
 * $Revision: 1.1 $
 * $Date: 2017-2-4 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.container;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.util.ClassUtil;

/**
 * <p>Title: PanelManager</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class PanelManager {
    private static final Map<String, BasePanel> context = new HashMap<String, BasePanel>();
    private static final Logger logger = LoggerFactory.getLogger(PanelManager.class);

    /**
     * default
     */
    private PanelManager() {
    }

    /**
     * @param panel
     */
    public static void setPanel(BasePanel panel) {
        if(panel != null) {
            context.put(panel.getClass().getName(), panel);
        }
    }

    /**
     * @param frame
     * @param className
     * @param create
     * @return BasePanel
     */
    public static BasePanel getPanel(BaseFrame frame, String className, boolean create) {
        BasePanel panel = context.get(className);

        if(panel == null && create == true) {
            panel = getInstance(frame, className);
            setPanel(panel);
        }
        return panel;
    }

    /**
     * @param className
     */
    public static void remove(String className) {
        context.remove(className);
    }

    /**
     * @param panel
     */
    public static void remove(BasePanel panel) {
        context.remove(panel.getClass().getName());
    }

    /**
     * @param frame
     * @param className
     * @return BasePanel
     */
    public static BasePanel getInstance(BaseFrame frame, String className) {
        try {
            Class<?> type = getClass(className);
            Constructor<?> constructor = type.getConstructor(new Class<?>[]{BaseFrame.class});
            return (BasePanel)(constructor.newInstance(frame));
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * @param className
     * @return Class<?>
     * @throws ClassNotFoundException
     */
    public static Class<?> getClass(String className) throws ClassNotFoundException {
        Class<?> clazz = null;

        try {
            clazz = Thread.currentThread().getContextClassLoader().loadClass(className);
        }
        catch(Exception e) {
        }

        if(clazz != null) {
            return clazz;
        }

        try {
            clazz = ClassUtil.class.getClassLoader().loadClass(className);
        }
        catch(Exception e) {
        }

        if(clazz != null) {
            return clazz;
        }
        return Class.forName(className);
    }
}
