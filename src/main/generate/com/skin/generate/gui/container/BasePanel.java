/*
 * $RCSfile: BasePanel.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-01-29 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.container;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

/**
 * <p>Title: BasePanel</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class BasePanel extends JPanel {
    private static final long serialVersionUID = 1L;
    protected BaseFrame frame;
    protected Map<String, Object> params;

    /**
     * @param frame
     */
    public BasePanel(BaseFrame frame) {
        this.frame = frame;
    }

    /**
     * @param params
     */
    public final void open(Map<String, Object> params) {
        if(this.params == null || this.params != params) {
            Map<String, Object> map = new HashMap<String, Object>();

            if(params != null) {
                map.putAll(params);
            }
            this.params = java.util.Collections.unmodifiableMap(map);
        }
        this.load(params);
    }

    /**
     * @param params
     */
    public abstract void load(Map<String, Object> params);

    /**
     * reload
     */
    public void reload() {
        this.open(this.params);
    }

    /**
     * @param message
     */
    public void message(String message) {
        javax.swing.JOptionPane.showMessageDialog(this, message, "Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * @param message
     * @return boolean
     */
    public boolean confirm(String message) {
        int returnValue = javax.swing.JOptionPane.showConfirmDialog(this, message, "Confirm", javax.swing.JOptionPane.YES_NO_OPTION);
        return (returnValue == javax.swing.JOptionPane.YES_OPTION);
    }

    /**
     * @param message
     */
    public void error(String message) {
        javax.swing.JOptionPane.showMessageDialog(this, message, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    /**
     * @param t
     */
    public void error(Throwable t) {
        javax.swing.JOptionPane.showMessageDialog(this, t.getMessage(), "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
    }
}
