/*
 * $RCSfile: DatabaseTreeNode.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.tree;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * <p>Title: DatabaseTreeNode</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class DatabaseTreeNode extends DefaultMutableTreeNode {
    private static final long serialVersionUID = 1L;
    private ImageIcon openIcon;
    private ImageIcon closeIcon;
    
    /**
     * @param object
     * @param icon
     */
    public DatabaseTreeNode(Object object, ImageIcon icon) {
        super(object);
        this.openIcon = icon;
        this.closeIcon = icon;
    }

    /**
     * @param object
     * @param openIcon
     * @param closeIcon
     */
    public DatabaseTreeNode(Object object, ImageIcon openIcon, ImageIcon closeIcon) {
        super(object);
        this.openIcon = openIcon;
        this.closeIcon = closeIcon;
    }

    /**
     * @return the openIcon
     */
    public ImageIcon getOpenIcon() {
        return this.openIcon;
    }

    /**
     * @param openIcon the openIcon to set
     */
    public void setOpenIcon(ImageIcon openIcon) {
        this.openIcon = openIcon;
    }

    /**
     * @return the closeIcon
     */
    public ImageIcon getCloseIcon() {
        return this.closeIcon;
    }

    /**
     * @param closeIcon the closeIcon to set
     */
    public void setCloseIcon(ImageIcon closeIcon) {
        this.closeIcon = closeIcon;
    }
}
