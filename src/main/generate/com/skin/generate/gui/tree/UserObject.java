/*
 * $RCSfile: UserObject.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-11-02 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.gui.tree;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: UserObject</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class UserObject {
    private String label;
    private String action;
    private Map<String, Object> parameters;

    /**
     * @param label
     */
    public UserObject(String label) {
        this.label = label;
    }

    /**
     * @param label
     * @param action
     */
    public UserObject(String label, String action) {
        this.label = label;
        this.action = action;
    }

    /**
     * @param label
     * @param action
     */
    public void reset(String label, String action) {
        this.label = label;
        this.action = action;

        if(this.parameters != null) {
            this.parameters.clear();
        }
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return this.action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @param name
     * @param value
     */
    public void setParameter(String name, Object value) {
        if(this.parameters == null) {
            this.parameters = new HashMap<String, Object>();
        }
        this.parameters.put(name, value);
    }

    /**
     * @param name
     * @return Object
     */
    public Object getParameter(String name) {
        if(this.parameters == null) {
            return null;
        }
        return this.parameters.get(name);
    }
    
    /**
     * @return Map<String, Object>
     */
    public Map<String, Object> getParameters() {
        if(this.parameters == null) {
            this.parameters = new HashMap<String, Object>();
        }
        return this.parameters;
    }

    /**
     * @param name
     * @return String
     */
    public String getString(String name) {
        return (String)(this.getParameter(name));
    }
    
    /**
     * @return String
     */
    @Override
    public String toString() {
        if(this.label != null) {
            return this.label;
        }
        else {
            return "unname";
        }
    }
}


