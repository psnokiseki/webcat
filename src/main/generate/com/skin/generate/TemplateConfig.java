/*
 * $RCSfile: TemplateConfig.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Title: TemplateConfig</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @version 1.0
 */
public class TemplateConfig {
    /**
     * @param dir
     * @return List<String>
     */
    public static List<String> getTemplateConfigList(String dir) {
        return getTemplateConfigList(new File(dir));
    }

    /**
     * @param dir
     * @return List<String>
     */
    public static List<String> getTemplateConfigList(File dir) {
        List<String> list = new ArrayList<String>();

        if(dir != null) {
            File[] files = dir.listFiles();
    
            if(files != null) {
                for(File file : files) {
                    if(file.isFile()) {
                        String name = file.getName();
    
                        if(name.toLowerCase().endsWith(".xml")) {
                            list.add(name);
                        }
                    }
                }
            }
        }
        return list;
    }
}
