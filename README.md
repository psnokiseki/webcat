#webcat
webcat是一个java版的web数据库管理工具，核心功能包括数据库的创建、编辑、sql分析、代码生成、数据备份等。
目前一期功能已开发完毕，所涉及到的功能我基本都已经测试通过，稳定版本为1.0.0.5。
接下来会继续完善一期功能，部分代码会重构，增加一些使用者提出的功能。
因此在此期间暂时关闭fork，待新版本开发完成并稳定之后再开放。代码会持续更新，敬请关注。
QQ群：341624652
有问题或者新需求请到QQ群反馈.
安装部署请参见下方。

**表结构编辑**
1. 表结构编辑之后将会根据数据库的约束条件和逻辑关系自动生成合适顺序的alter语句执行, 不需要人为控制编辑顺序。
2. 新建表支持导入建表脚本，编辑之后再保存。
3. 目前的表结构编辑仅支持Mysql语法。
![输入图片说明](http://git.oschina.net/uploads/images/2016/1105/234352_c6eef39a_615195.jpeg "表结构编辑")

**数据库表列表页面**
![输入图片说明](http://git.oschina.net/uploads/images/2016/1105/234445_3faaa6be_615195.jpeg "数据库表列表页面")

**SQL查询页面**
![输入图片说明](http://git.oschina.net/uploads/images/2016/1105/234503_68f15263_615195.jpeg "SQL查询页面")

**代码生成**
1. 可以在表结构编辑页面对字段进行调整。
2. 模板引擎采用ayada，可以采用jsp和jstl语法编写，对空格和换行的处理经过专门优化，真正的所见即所得。
3. 支持批量生成。
4. 代码生成可支持任意数据库。
![输入图片说明](http://git.oschina.net/uploads/images/2016/1105/234541_4e4ea17e_615195.jpeg "代码生成")

**代码生成-模板编辑**
1. 模板配置支持同时配置任意多个模板，此处可以再次对模板参数进行调整。
![输入图片说明](http://git.oschina.net/uploads/images/2016/1105/234601_8ed934ba_615195.jpeg "代码生成-模板编辑")

**直接从SQL文件解析生成代码**
1. 代码生成支持从sql文件解析表结构，仅支持Mysql语法。
![输入图片说明](http://git.oschina.net/uploads/images/2016/1105/234617_fc72d975_615195.jpeg "直接从SQL文件解析生成代码")

**支持本地图形界面生成代码，不需要部署web应用**
图形界面请运行gui/startup.bat
![单表生成](http://git.oschina.net/uploads/images/2017/0130/161610_7c5f92ac_615195.jpeg "单表生成")
![批量生成](http://git.oschina.net/uploads/images/2017/0130/161625_7d25de79_615195.jpeg "批量生成")

运行部署
=================
1. 第一种方式，将release/webcat.war部署到tomcat。也可以使用build.bat自己编译war包并部署到tomcat。
2. 第二种方式，将项目导入到eclipse，编译之后将webapp目录配置到tomcat中.
3. 第三种方式，修改tomcat.bat，将里面tomcat的路径修改为你的tomcat的安装路径，然后将你的tomcat/conf/server.xml覆盖
   到项目的conf/server.xml，然后参考原来的conf/server.xml适当修改。最后双击tomcat.bat即可启动项目。

**相关依赖**
1. 视图层采用的是jsp，但不是由容器的jsp引擎执行的，使用ayada模版引擎，参见http://git.oschina.net/xuesong123/jsp-jstl-engine.
2. 文件管理采用另一个开源项目，参见 http://git.oschina.net/xuesong123/finder-web

用户控制
=================
webcat集成了finder，用户控制由finder提供，具体请参考 http://git.oschina.net/xuesong123/finder-web
默认的用户名密码: admin 1234
如果你需要更加强大的用户控制，请参考finder自行实现。


数据库
=================
数据库建库脚本在db目录。这个是webcat自己使用的数据库，给定时任务用。
一般情况下用不到数据库。
如果你不想让webcat使用数据库，删除或者重命名WEB-INF/classes/jdbc.properties文件即可。


常见问题
=================
1. 中文乱码
请在tomcat的server.xml中配置URIEncoding:
``
<Connector port="80" protocol="HTTP/1.1"
    connectionTimeout="20000"
    redirectPort="8443" URIEncoding="UTF-8"/>
``

2. 'javax.tools.JavaCompiler' not found
   webcat使用的ayada模版引擎，编译模式要求必须加载tools.jar, 所以在环境变量里面配置%JAVA_HOME%\lib\tools.jar即可。


其他说明
=================
1. sql脚本放在什么地方？
    sql脚本必须放在WEB-INF/sqls目录。

2. 如果我不想把sql脚本放到WEB-INF/sqls目录内怎么办？
    在WEB-INF/sqls目录新建任意文件，修改扩展名为.link.sql，例如: mytest.link.sql，在mytest.link.sql文件内写入你的sql脚本的路径即可。

3. 我想自己实现用户控制，如何屏蔽掉finder提供的用户控制功能？请在web.xml中的DispatchFilter中配置excludes参数, 排除掉用户控制功能，并删除SessionFilter。
   参数值为：
   com.skin.finder.action.LoginAction.execute,
   com.skin.finder.action.LogoutAction.execute,
   com.skin.finder.action.SimpleUserAction.add,
   com.skin.finder.action.SimpleUserAction.save

# README
建议不要fork代码，fork之后我这里更新之后你那里不会同步更新，建议watch或者star，都可以收到最新更新。

ChangeLog
=================
$# ........................................................
$# version: 1.0.0.5
$# download: http://git.oschina.net/xuesong123/webcat
$## change log
$## 1. sql文件以目录结构列出;
$## 2. 增加解析insert语句并将数据以表格的形式显示;
$## 3. 其他bug修复;
$# ........................................................
