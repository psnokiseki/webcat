<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Webcat</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/generate/table-list.js"></script>
</head>
<body style="overflow: hidden;" filePath="${filePath}" connectionName="${connectionName}" templateConfig="${templateConfig}">
<c:if test="${util.isEmpty(filePath)}">
    <c:set var="targetUrl" value="/table/edit.html?connectionName=${connectionName}"/>
</c:if>
<c:if test="${util.notEmpty(filePath)}">
    <c:set var="targetUrl" value="/sql/edit.html?filePath=${URLUtil.encode(filePath)}"/>
</c:if>
<div class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">批量生成</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a id="tools-btn" class="button" href="javascript:void(0)">常用工具</a>
                <a id="batch-btn" class="button" href="javascript:void(0)">批量生成</a>
                <a id="finder-btn" class="button" href="javascript:void(0)" target="_blank">查看文件</a>
                <a id="export-script-btn" class="button" href="javascript:void(0)">导出脚本</a>
                <c:if test="${util.notEmpty(connectionName)}">
                <a id="export-data-btn" class="button" href="javascript:void(0)">导出数据</a>
                </c:if>
            </div>
            <div class="resize-d">
                <table class="table">
                    <tr class="thead">
                        <td class="w40"><input type="checkbox" checked="true" title="全 选" onclick="Util.check('tableName', this.checked)"/></td>
                        <td class="w200">表名</td>
                        <td class="w300">描述</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach items="${tableList}" var="table" varStatus="status">
                    <c:set var="tableId" value="drag_target_${table.tableName}"/>
                    <tr>
                        <td class="w60 center"><input type="checkbox" name="tableName" value="${table.tableName}" tableType="${table.tableType}" checked="true"/></td>
                        <td>
                            <img src="/resource/webcat/images/table.gif"/>
                            <!-- a href="${targetUrl}&templateConfig=${URLUtil.encode(templateConfig)}&tableName=${URLUtil.encode(table.tableName)}" title="${connectionConfig.url}">${table.tableName}</a -->
                            <a href="javascript:void(0)" onclick="DomUtil.show('table-panel-${table.tableName}');" title="表结构">${table.tableName}</a -->
                        </td>
                        <td>${table.remarks}</td>
                        <td>
                            <c:if test="${util.isEmpty(filePath)}">
                            <a href="/generate/table/edit.html?connectionName=${URLUtil.encode(connectionName)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">生成代码</a>
                            </c:if>
                            <c:if test="${util.notEmpty(filePath)}">
                            <a href="/webcat/sql/edit.html?path=${URLUtil.encode(filePath)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">生成代码</a>
                            </c:if>
                        </td>
                    </tr>
                    </c:forEach>
                </table>
                <div class="h60"></div>
            </div>
        </div>
    </div>
</div>
<c:forEach items="${tableList}" var="table" varStatus="status">
    <c:set var="tableId" value="drag_target_${table.tableName}"/>
    <div id="table-panel-${table.tableName}" class="dialog hide" style="position: absolute; top: 20px; left: 20px;">
        <div class="panel">
            <div class="panel-title" dragable="true">
                <h4>${table.tableName}</h4>
                <span class="close"></span>
            </div>
            <div class="panel-content" style="width: 840px; height: 360px; overflow: auto; cursor: default;">
                <table class="table" style="width: 800px;">
                    <tr class="thead">
                        <td class="cc w60 bb">index</td>
                        <td class="w200 bb">columnName</td>
                        <td class="w200 bb">columnType</td>
                        <td class="bb">remarks</td>
                    </tr>
                <c:forEach items="${table.getColumns()}" var="column" varStatus="status">
                    <tr title="${column.typeName}: ${column.remarks}">
                        <td class="cc bb">${status.index + 1}</td>
                        <td class="bb">${column.columnName}</td>
                        <td>${column.typeName}(${column.precision})</td>
                        <td>${column.remarks}</td>
                    </tr>
                    </c:forEach>
                </table>
                <div style="height: 30px;"></div>
            </div>
        </div>
    </div>
</c:forEach>
<div class="hide">
    <form name="exportForm"></form>
</div>
</body>
</html>