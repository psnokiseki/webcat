<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/css/form.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript">
<!--
var StringUtil = {};
var DateFormat = {};

StringUtil.trim = function(s){return (s != null ? s.toString().replace(/(^\s*)|(\s*$)/g, "") : "");};
DateFormat.ltrim = function(s, x) {
    var value = "";

    if(s != null) {
        var i = 0;

        for(var i = 0; i < s.length; i++) {
            var ch = s.charAt(i);

            if(ch != x) {
                break;
            }
        }
        return s.substring(i);
    }
    return value;
};

DateFormat.format = function(date, pattern){
    if(date == null) {
        return "";
    }

    var dateTime = this.getDateTime(date);
    var cs = ["y", "M", "d", "H", "m", "s", "S"];
    var fs = pattern.split("");
    var ds = dateTime.split("");

    var y = 3;
    var M = 6;
    var d = 9;
    var H = 12;
    var m = 15;
    var s = 18;
    var S = 22;

    for(var i = fs.length - 1; i > -1; i--) {
        switch (fs[i]) {
            case cs[0]: {
                fs[i] = ds[y--];
                break;
            }
            case cs[1]: {
                fs[i] = ds[M--];
                break;
            }
            case cs[2]: {
                fs[i] = ds[d--];
                break;
            }
            case cs[3]: {
                fs[i] = ds[H--];
                break;
            }
            case cs[4]: {
                fs[i] = ds[m--];
                break;
            }
            case cs[5]: {
                fs[i] = ds[s--];
                break;
            }
            case cs[6]: {
                fs[i] = ds[S--];
                break;
            }
        }
    }
    return fs.join("");
};

DateFormat.parse = function(dateTime, pattern){
    if(dateTime == null || dateTime == "") {
        return null;
    }

    var y = "";
    var M = "";
    var d = "";
    var H = "";
    var m = "";
    var s = "";
    var S = "";

    var cs = ["y", "M", "d", "H", "m", "s", "S"];
    var ds = pattern.split("");
    var size = Math.min(ds.length, dateTime.length);

    for(var i = 0; i < size; i++) {
        switch(ds[i]) {
            case cs[0]: {
                y += dateTime.charAt(i);
                break;
            }
            case cs[1]: {
                M += dateTime.charAt(i);
                break;
            }
            case cs[2]: {
                d += dateTime.charAt(i);
                break;
            }
            case cs[3]: {
                H += dateTime.charAt(i);
                break;
            }
            case cs[4]: {
                m += dateTime.charAt(i);
                break;
            }
            case cs[5]: {
                s += dateTime.charAt(i);
                break;
            }
            case cs[6]: {
                S += dateTime.charAt(i);
                break;
            }
        }
    }

    y = this.ltrim(y, "0");
    M = this.ltrim(M, "0");
    d = this.ltrim(d, "0");
    H = this.ltrim(H, "0");
    m = this.ltrim(m, "0");
    s = this.ltrim(s, "0");
    S = this.ltrim(S, "0");

    if(y.length < 1) y = 0; else y = parseInt(y, 10);
    if(M.length < 1) M = 0; else M = parseInt(M, 10);
    if(d.length < 1) d = 0; else d = parseInt(d, 10);
    if(H.length < 1) H = 0; else H = parseInt(H, 10);
    if(m.length < 1) m = 0; else m = parseInt(m, 10);
    if(s.length < 1) s = 0; else s = parseInt(s, 10);
    if(S.length < 1) S = 0; else S = parseInt(S, 10);

    if(isNaN(y) || isNaN(M) || isNaN(d) || isNaN(H) || isNaN(m) || isNaN(s) || isNaN(S)) {
        return null;
    }
    else {
        return new Date(y, M - 1, d, H, m, s, S);
    }
};

DateFormat.getDate = function(date){return this.format(date, "yyyy-MM-dd");};
DateFormat.getTime = function(date){return this.format(date, "HH:mm:ss SSS");};
DateFormat.getDateTime = function(date){
    if(date == null) {
        date = new Date();
    }

    var y = date.getFullYear();
    var M = date.getMonth() + 1;
    var d = date.getDate();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var S = date.getTime() % 1000;

    var a = [];

    a[a.length] = y.toString();
    a[a.length] = "-";

    if(M < 10) {
        a[a.length] = "0";
    }

    a[a.length] = M.toString();
    a[a.length] = "-";

    if(d < 10) {
        a[a.length] = "0";
    }

    a[a.length] = d.toString();

    a[a.length] = " ";
    
    if(h < 10) {
        a[a.length] = "0";
    }

    a[a.length] = h.toString();
    a[a.length] = ":";

    if(m < 10) {
        a[a.length] = "0";
    }

    a[a.length] = m.toString();
    a[a.length] = ":";

    if(s < 10) {
        a[a.length] = "0";
    }

    a[a.length] = s.toString();
    a[a.length] = " ";

    if(S < 100) {
        a[a.length] = "0";
    }

    if(S < 10) {
        a[a.length] = "0";
    }

    a[a.length] = S.toString();
    return a.join("");
};

jQuery(function() {
    jQuery("input[name=input]").change(function() {
        jQuery("#convert-btn").click();
    });

    jQuery("#current-btn").click(function() {
        jQuery("input[name=input]").val(DateFormat.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        jQuery("#convert-btn").click();
    });

    jQuery("#convert-btn").click(function() {
        var input = jQuery("input[name=input]").val();

        if(input == null) {
            jQuery("input[name=datetime]").val("NaN");
            jQuery("input[name=timestamp]").val("NaN");
            return;
        }

        var date = null;

        if(isNaN(input)) {
            if(input.length == "yyyy-MM-dd".length) {
                date = DateFormat.parse(input, "yyyy-MM-dd");
            }
            else if(input.length == "yyyy-MM-dd HH:mm:ss".length) {
                date = DateFormat.parse(input, "yyyy-MM-dd HH:mm:ss");
            }
            else {
                date = null;
            }
        }
        else {
            date = new Date();
            date.setTime(parseInt(input));
        }

        if(date == null) {
            jQuery("input[name=datetime]").val("NaN");
            jQuery("input[name=timestamp]").val("NaN");
            return;
        }

        jQuery("input[name=timestamp]").val(date.getTime());
        jQuery("input[name=datetime]").val(DateFormat.format(date, "yyyy-MM-dd HH:mm:ss"));
    });
    jQuery("#current-btn").click();
});
//-->
</script>
</head>
<body>
<div style="margin: 0px auto 0px auto; width: 1000px;">
    <div class="h20"></div>
    <div class="form">
        <div class="form-row">
            <div class="form-label">输入时间：</div>
            <div class="form-c300">
                <div class="form-field">
                    <input name="input" type="text" class="form-text" value="">
                </div>
            </div>
            <div class="form-m300">
                <div class="form-comment">输入格式: 1490455665 或者 2017-03-26 或者 2017-03-26 08:00:00。</div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-label">转换结果：</div>
            <div class="form-c300">
                <div class="form-field">
                    <input name="timestamp" type="text" class="form-text" value="">
                </div>
                <div class="form-field">
                    <input name="datetime" type="text" class="form-text" value="">
                </div>
            </div>
            <div class="form-m300">
                <div class="form-comment"></div>
            </div>
        </div>
        <div class="button">
            <button id="current-btn" class="button">当前时间</button>
            <button id="convert-btn" class="button ensure">转换</button>
        </div>
    </div>
</div>
</body>
</html>