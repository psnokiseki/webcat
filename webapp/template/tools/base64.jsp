<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/css/form.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/base64.js"></script>
<script type="text/javascript">
<!--
jQuery(function() {
    var getSource = function() {
        return document.getElementById("source").value;
    };

    var setResult = function(value) {
        document.getElementById("source").value = value;
    };

    jQuery("#url-encode-btn").click(function() {
        var source = getSource();
        var result = encodeURIComponent(source);
        setResult(result);
    });

    jQuery("#url-decode-btn").click(function() {
        var source = getSource();
        var result = decodeURIComponent(source);
        setResult(result);
    });

    jQuery("#base64-encode-btn").click(function() {
        var source = getSource();
        var result = Base64.encode(source);
        setResult(result);
    });

    jQuery("#base64-decode-btn").click(function() {
        var source = getSource();
        var result = Base64.decode(source);
        setResult(result);
    });

    jQuery("#escape-btn").click(function() {
        var source = getSource();
        var result = escape(source);
        setResult(result);
    });

    jQuery("#unescape-btn").click(function() {
        var source = getSource();
        var result = unescape(source);
        setResult(result);
    });
});
//-->
</script>
</head>
<body>
<div style="margin: 0px auto 0px auto; width: 1000px;">
    <div class="h20"></div>
    <div class="form">
        <div><textarea id="source" style="width: 100%; height: 400px;"></textarea></div>
        <div class="button">
            <button id="url-encode-btn" class="button">URL编码</button>
            <button id="url-decode-btn" class="button">URL解码</button>
            <!--
            <button class="button">HTML编码</button>
            <button class="button">HTML解码</button>
            -->
            <button id="base64-encode-btn" class="button">Base64编码</button>
            <button id="base64-decode-btn" class="button">Base64解码</button>

            <button id="escape-btn" class="button">escape</button>
            <button id="unescape-btn" class="button">unescape</button>
        </div>
    </div>
</div>
</body>
</html>