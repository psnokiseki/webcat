<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>blank</title>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/css/browser.css"/>
</head>
<body>
<div style="margin: 0px auto 0px auto; width: 1000px;">
    <div style="margin-top: 20px; margin-left: 20px; color: #c0c0c0;">
        <a href="${contextPath}/webcat/tools.html" target="_blank">http://${localIp}/webcat/tools.html</a>
    </div>
    <div class="outline-view" style="border: none;">
        <ul>
            <li class="item">
                <div class="item-box">
                    <div class="item-name">
                        <a class="icon" href="${contextPath}/webcat/tools/sql/create.html"><img src="${contextPath}/resource/webcat/images/icon1.gif"/></a>
                        <h3><a href="${contextPath}/webcat/tools/sql/create.html">[SQL] CREATE编辑</a></h3>
                        <h5 class="item-type">[小工具]</h5>
                    </div>
                    <div class="item-desc">输入create语句, 编辑生成insert语句或者json.</div>
                </div>
            </li>
            <li class="item">
                <div class="item-box">
                    <div class="item-name">
                        <a class="icon" href="${contextPath}/webcat/tools/sql/insert.html"><img src="${contextPath}/resource/webcat/images/icon1.gif"/></a>
                        <h3><a href="${contextPath}/webcat/tools/sql/insert.html">[SQL] INSERT编辑</a></h3>
                        <h5 class="item-type">[小工具]</h5>
                    </div>
                    <div class="item-desc">输入insert语句, 编辑生成insert语句或者json.</div>
                </div>
            </li>
            <li class="item">
                <div class="item-box">
                    <div class="item-name">
                        <a class="icon" href="${contextPath}/webcat/tools/timestamp.html"><img src="${contextPath}/resource/webcat/images/icon1.gif"/></a>
                        <h3><a href="${contextPath}/webcat/tools/timestamp.html">时间转换</a></h3>
                        <h5 class="item-type">[小工具]</h5>
                    </div>
                    <div class="item-desc">中文习惯的时间格式转换器.</div>
                </div>
            </li>
            <li class="item">
                <div class="item-box">
                    <div class="item-name">
                        <a class="icon" href="${contextPath}/webcat/tools/base64.html"><img src="${contextPath}/resource/webcat/images/icon1.gif"/></a>
                        <h3><a href="${contextPath}/webcat/tools/base64.html">Base64编解码</a></h3>
                        <h5 class="item-type">[小工具]</h5>
                    </div>
                    <div class="item-desc">Base64编解码.</div>
                </div>
            </li>
        </ul>
    </div>
</div>
</body>
</html>
