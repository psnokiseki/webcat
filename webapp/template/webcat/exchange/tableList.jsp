<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>数据备份</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript">
<!--
var Webcat = {};

Webcat.showError = function(message) {
    jQuery("#filePanel").html("<div style=\"color: #ff0000;\">" + message + "</div>");
};

Webcat.showProgress = function(token) {
    jQuery.ajax({
        "type": "post",
        "url": "${contextPath}/webcat/backup/getProgress.html",
        "data": "token=" + token,
        "dataType": "json",
        "error": function() {
            Webcat.showError("系统错误，请稍后再试！");
        },
        "success": function(result) {
            Response.success(result, function(progress) {
                if(progress == null) {
                    Webcat.showError("系统错误，请稍后再试！");
                    return;
                }

                jQuery("#progressPanel").show();
                jQuery("#progress-bar").css("width", progress.progress + "%");

                if(progress.table != null) {
                    jQuery("#progressPanel span.info").html("进度：" + progress.progress + "%，当前正在备份：" + progress.table);
                }
                else {
                    jQuery("#progressPanel span.info").html("进度：" + progress.progress + "%，完成。");
                }

                if(progress.progress < 100) {
                    setTimeout(function() {
                        Webcat.showProgress(token);
                    }, 1000);
                }
                else {
                    jQuery("#filePanel").html("备份完成：<a href=\"${contextPath}/webcat/backup/detail.html?zip=" + progress.file + "\" title=\"点击查看详情\">" + progress.file + "</a>");
                }
            });
        }
    });
};

Webcat.backup = function(name, database, tableNameList) {
    var params = {"name": name, "database": database, "tableName": tableNameList};
    jQuery("#tablePanel").hide();
    jQuery("#progressPanel").show();
    jQuery("#progress-bar").css("width", "0%");

    jQuery.ajax({
        "type": "post",
        "url": "${contextPath}/webcat/backup/execute.html",
        "data": jQuery.param(params, true),
        "dataType": "json",
        "error": function() {
            alert("系统错误，请稍后再试！");
        },
        "success": function(result) {
            Response.success(result, function(value) {
                Webcat.showProgress(value);
            });
        }
    });
};

jQuery(function(){
    jQuery("input[name=checkAll]").click(function(){
        var checked = this.checked;
        jQuery("input[name=tableName]").each(function(){
            this.checked = checked;
        });
    });

    jQuery("#back-btn").click(function() {
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        var contextPath = PageContext.getContextPath();
        window.location.href = contextPath + "/webcat/table/list.html"
            + "?name=" + encodeURIComponent(name)
            + "&database=" + encodeURIComponent(database);
    });

    jQuery("#do-backup-btn").click(function(){
        var tableNameList = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");

        jQuery("input[name=tableName]").each(function(){
            if(this.checked == true) {
                tableNameList[tableNameList.length] = this.value;
            }
        });

        if(tableNameList.length < 1) {
            alert("请选择要备份的表 !");
            return;
        }
        Webcat.backup(name, database, tableNameList);
    });

    jQuery("#backup-list-btn").click(function() {
        var contextPath = PageContext.getContextPath();
        window.location.href = contextPath + "/webcat/backup/list.html";
    });
});

jQuery(function() {
    var tabPanel = new TabPanel({"container": "table-list-panel"});
});
//-->
</script>
</head>
<body>
<div id="table-list-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">数据备份</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a id="back-btn" class="button" href="javascript:void(0)">返回</a>
                <a id="do-backup-btn" class="button" href="javascript:void(0)">备份</a>
                <a id="backup-list-btn" class="button" href="javascript:void(0)">历史备份</a>
            </div>
            <div class="list-panel">
                <div id="tablePanel">
                    <table class="list">
                        <tr class="head">
                            <td class="w60"><input name="checkAll" type="checkbox"/></td>
                            <td class="w60">序号</td>
                            <td>表 名</td>
                        </tr>
                        <c:forEach items="${tableList}" var="table" varStatus="status">
                        <tr>
                            <td class="w60 center"><input name="tableName" type="checkbox" value="${table.tableName}"/></td>
                            <td>${status.index + 1}</td>
                            <td>${table.tableName}</td>
                        </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="progressPanel" class="hide">
    <div style="margin: 30px auto 0px auto; width: 80%;">
        <div style="height: 30px; border: 1px solid #166e03; position: realtive;">
            <div id="progress-bar" style="position: realtive; top: 0px; left: 0px; width: 100%; height: 30px; background-color: #28c204;"></div>
        </div>
        <div style="margin-top: 10px; line-height: 20px; font-size: 12px; color: #999999;">
            <span class="info"></span>
        </div>
        <div id="filePanel" style="margin-top: 20px;"></div>
    </div>
</div>
<div id="pageContext" class="hide" connectionName="${name}" database="${database}"></div>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>