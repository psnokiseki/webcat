<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>Webcat v1.0</title>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/css/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/htree/css/htree.css"/>
<script type="text/javascript" src="${contextPath}/resource/htree/htree.js"></script>
<script type="text/javascript" src="${contextPath}/resource/htree/htree.util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
//<![CDATA[
HTree.treeNodeOnClick = function(url) {
    if(url != null && url.length > 0) {
        window.top.mainFrame.location.href = url;
    }
};

function buildTree(id, url){
    HTree.config.stylePath = "${contextPath}/resource/htree/database/";
    var tree = new HTree.TreeNode({text: "webcat", href: "javascript:void(0)"});
    tree.load(url, function(){
        this.render(document.getElementById(id));
    });
}

jQuery(function() {
    jQuery("div.resize-d").change(function() {
        var offset = jQuery(this).offset();
        var clientHeight = document.documentElement.clientHeight;
        jQuery(this).height(clientHeight - offset.top - 10);
    });

    jQuery(window).bind("resize", function() {
        jQuery("div.resize-d").change();
    });
    jQuery("div.resize-d").change();
});

jQuery(function() {
    setTimeout(function() {
        buildTree("htree", "${contextPath}/webcat/getConnectionXml.html");
    }, 100);
});
//]]>
</script>
</head>
<body>
<div class="left-nav">
    <div class="menu-body" style="padding-left: 8px;">
        <div id="htree" class="htree resize-d" style="padding-top: 10px; white-space: nowrap; overflow: scroll;"></div>
    </div>
</div>
</body>
</html>
