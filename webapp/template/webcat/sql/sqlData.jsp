<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Webcat</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript">
<!--
jQuery(function() {
    jQuery(window).resize();
});
//-->
</script>
</head>
<body style="overflow: hidden;">
<div id="table-list-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active" title="${filePath}"><span class="label">SQL [${fileName}]</span></li>
        </ul>
    </div>
    <div class="tab-panel" style="display: block;">
        <div class="menu-bar"></div>
        <div class="resize-d">
            <table class="list form">
                <tr class="head">
                    <c:forEach items="${sqlResult.columns}" var="column" varStatus="status">
                        <td>${column.columnName}</td>
                    </c:forEach>
                </tr>
                <c:forEach items="${sqlResult.records}" var="record" varStatus="status">
                <tr>
                    <c:forEach items="${record.values}" var="item">
                    <td>${item}</td>
                    </c:forEach>
                </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<div id="pageContext" class="hide" filePath="${filePath}"></div>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>
