<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Webcat</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sql-table-list.js"></script>
</head>
<body style="overflow: hidden;">
<div id="table-list-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active" title="${filePath}"><span class="label">${fileName}</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar"></div>
            <div class="resize-d">
                <table class="table">
                    <tr class="thead">
                        <td class="w200">名字</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach items="${fileList}" var="file" varStatus="status">
                    <c:if test="${file.isDirectory()}">
                    <tr>
                        <td>
                            <span style="float: left; width: 18px; display: inline-block;"><img src="${contextPath}/resource/webcat/images/folder.gif"></span>${file.getName()}
                        </td>
                        <td style="padding-left: 10px;">
                            <a href="${contextPath}/webcat/sql/fileList.html?path=${filePath}/${file.getName()}">打开目录</a>
                        </td>
                    </tr>
                    </c:if>
                    </c:forEach>

                    <c:forEach items="${fileList}" var="file" varStatus="status">
                    <c:if test="${file.isFile()}">
                    <tr>
                        <td>
                            <span style="float: left; width: 18px; display: inline-block;"><img src="${contextPath}/resource/webcat/images/sql.gif"></span>${file.getName()}
                        </td>
                        <td style="padding-left: 10px;">
                            <a href="${contextPath}/webcat/sql/list.html?path=${filePath}/${file.getName()}" title="只针对create语句">打 开</a>
                            <a href="${contextPath}/webcat/sql/data.html?path=${filePath}/${file.getName()}" title="只针对insert语句">查看数据</a>
                        </td>
                    </tr>
                    </c:if>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="pageContext" class="hide" contextPath="${contextPath}" filePath="${filePath}"></div>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>
