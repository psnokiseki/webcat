<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Webcat</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/codemirror-5.21.0/lib/codemirror.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/codemirror-5.21.0/theme/mysql.css"/>
<script type="text/javascript" src="${contextPath}/resource/codemirror-5.21.0/lib/codemirror.js"></script>
<script type="text/javascript" src="${contextPath}/resource/codemirror-5.21.0/mode/sql/sql.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sqlplus.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/db-list.js"></script>
</head>
<body style="overflow: hidden;">
<div id="db-list-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">Database[${name}]</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a id="create-db-btn" class="button" href="javascript:void(0)">新建数据库</a>
            </div>
            <div class="resize-d">
                <table id="db-list" class="list form">
                    <tr class="head">
                        <td class="w40">&nbsp;</td>
                        <td class="w200">数据库</td>
                        <td class="w100">字符集</td>
                        <td class="w100">容量(MB)</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach items="${databaseList}" var="database" varStatus="status">
                    <tr>
                        <td>${status.index + 1}</td>
                        <td class="bb">${database}</td>
                        <td style="padding-right: 20px; text-align: right;">--</td>
                        <td style="padding-right: 20px; text-align: right;">--</td>
                        <td style="padding-left: 20px;">
                            <a class="btn open" href="${contextPath}/webcat/table/list.html?name=${name}&database=${database}">打 开</a>
                            <a class="btn remove" href="javascript:void(0)" database="${database}">删 除</a>
                        </td>
                    </tr>
                    </c:forEach>
                </table>
                <div class="h60"></div>
            </div>
        </div>
    </div>
</div>
<div id="alter-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>Alter Sql</h4>
            <span class="close"></span>
        </div>
        <div class="menu-bar">
            <a class="button select" href="javascript:void(0)">全 选</a>
        </div>
        <div class="panel-content" style="width: 800px; overflow: hidden; cursor: default;">
            <pre class="sql-editor" style="width: 790px; height: 240px;" contenteditable="true"></pre>
            <div class="operator">
                <input type="button" class="button ensure" value="执 行"/>
                <input type="button" class="button cancel" value="取 消"/>
            </div>
        </div>
    </div>
</div>

<div id="db-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>创建数据库</h4>
            <span class="close"></span>
        </div>
        <div class="panel-content" style="width: 600px; background-color: #ffffff; overflow: hidden; cursor: default;">
            <div class="form" style="padding: 20px;">
                <div class="row">
                    <div class="label">数据库名:</div>
                    <div class="c300">
                        <div class="field">
                            <input name="database" type="text w200" class="text" style="width: 256px;" placeholder="数据库名"/>
                            <span class="required">*</span>
                        </div>
                    </div>
                    <div class="m300">
                        <div class="comment">&nbsp;</div>
                    </div>
                </div>
                <div class="row">
                    <div class="label">字符集:</div>
                    <div class="c300">
                        <div class="field">
                            <select name="charset" style="width: 256px;"></select>
                        </div>
                    </div>
                    <div class="m300">
                        <div class="comment">&nbsp;</div>
                    </div>
                </div>
                <div class="row">
                    <div class="label">Collation:</div>
                    <div class="c300">
                        <div class="field">
                            <select name="collation" style="width: 256px;"></select>
                        </div>
                    </div>
                    <div class="m300">
                        <div class="comment">&nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="operator">
                <input type="button" class="button ensure" value="确 定"/>
                <input type="button" class="button cancel" value="取 消"/>
            </div>
        </div>
    </div>
</div>
<div id="pageContext" class="hide" contextPath="${contextPath}" connectionName="${name}"></div>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>
