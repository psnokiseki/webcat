<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Webcat</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/codemirror-5.21.0/lib/codemirror.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/codemirror-5.21.0/theme/mysql.css"/>
<script type="text/javascript" src="${contextPath}/resource/codemirror-5.21.0/lib/codemirror.js"></script>
<script type="text/javascript" src="${contextPath}/resource/codemirror-5.21.0/mode/sql/sql.js"></script>

<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sqlplus.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/table-list.js"></script>
</head>
<body style="overflow: hidden;">
<div id="table-list-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">DBM [${database}]</span></li>
        </ul>
        <span class="add" title="打开新的标签页"></span>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a id="create-table-btn" class="button" href="javascript:void(0)">新建表</a>
                <a id="import-table-btn" class="button" href="javascript:void(0)">导入向导</a>
                <a id="export-table-btn" class="button" href="javascript:void(0)">导出向导</a>
                <a id="backup-table-btn" class="button" href="javascript:void(0)">数据备份</a>
            </div>
            <div class="resize-d">
                <table id="table-list" class="list form">
                    <tr class="head">
                        <td class="w40">&nbsp;</td>
                        <td class="w200">表名</td>
                        <td class="w100">字符集</td>
                        <td class="w100">容量(MB)</td>
                        <td class="w300">描述</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach items="${tableList}" var="table" varStatus="status">
                    <tr>
                        <td>${status.index + 1}</td>
                        <td class="bb">${table.tableName}</td>
                        <td style="padding-right: 20px; text-align: right;">--</td>
                        <td style="padding-right: 20px; text-align: right;">--</td>
                        <td>${table.remarks}</td>
                        <td style="padding-left: 20px;">
                            <a class="btn query" href="javascript:void(0)" tableName="${table.tableName}">查 询</a>
                            <a class="btn edit"  href="javascript:void(0)" tableName="${table.tableName}">编 辑</a>
                            <a class="delete"    href="javascript:void(0)" tableName="${table.tableName}">删 除</a>
                            <a class="insert"    href="javascript:void(0)" tableName="${table.tableName}">插入编辑</a>
                            <a class="generate"  href="javascript:void(0)" tableName="${table.tableName}">代码生成</a>
                        </td>
                    </tr>
                    </c:forEach>
                </table>
                <div class="h60"></div>
            </div>
        </div>
    </div>
</div>
<div id="alter-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>Alter Sql</h4>
            <span class="close"></span>
        </div>
        <div class="menu-bar">
            <a class="button select" href="javascript:void(0)">全 选</a>
        </div>
        <div class="panel-content" style="width: 800px; overflow: hidden; cursor: default;">
            <pre class="sql-editor" style="width: 790px; height: 240px;" contenteditable="true"></pre>
            <div class="operator">
                <input type="button" class="button ensure" value="执 行"/>
                <input type="button" class="button cancel" value="取 消"/>
            </div>
        </div>
    </div>
</div>
<div id="pageContext" class="hide" contextPath="${contextPath}" connectionName="${name}" database="${database}"></div>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>
