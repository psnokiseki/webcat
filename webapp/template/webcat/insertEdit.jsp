<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/css/dbtype.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sql-insert2.js"></script>
</head>
<body style="overflow: hidden;">
<div class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">插入语句编辑</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="panel">
                <div class="menu-bar">
                    <a id="tools-box" class="button" href="${contextPath}/webcat/tools.html">工具箱</a>
                    <a id="insert-btn" class="button">生 成</a>
                </div>
            </div>
            <div class="form-panel">
                <textarea id="result" class="editor h60" readonly="true"></textarea>
            </div>
            <div class="list-panel">
                <div>
                    <table class="table" style="width: 1000px;">
                        <tr class="thead">
                            <td class="w30">&nbsp;</td>
                            <td class="w200">列名</td>
                            <td class="w100">类型</td>
                            <td class="w100">可空</td>
                            <td class="w300">值</td>
                            <td class="center">操 作</td>
                        </tr>
                    </table>
                </div>
                <div id="columnPanel" style="height: 360px; padding-top: 0px; overflow-x: hidden; overflow-y: auto;">
                    <table id="columnList" class="table" style="width: 1000px;"></table>
                    <div class="h60"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="edit-panel" class="dialog hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4><span class="icon-table"></span>编辑字段</h4>
            <span class="close"></span>
        </div>
        <div class="panel-content">
            <div class="form-panel" style="width: 600px; height: 300px; overflow: auto; cursor: default;">
                <textarea name="content" class="text" style="width: 592px; height: 290px; border: 1px solid #c0c0c0; outline: none; overflow: auto;"></textarea>
            </div>
        </div>
        <div style="margin: 20px 10px 20px 0px; text-align: right;">
            <input name="ensure" type="image" src="/resource/webcat/images/ensure.gif"/>
            <input name="cancel" type="image" src="/resource/webcat/images/cancel.gif"/>
        </div>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
var table = <c:out value="${JsonUtil.stringify(table)}"/>;
//]]>
</script>
</body>
</html>