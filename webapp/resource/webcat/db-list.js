jQuery(function() {
    var tabPanel = new SqlPlusPanel({"container": "db-list-panel"});

    jQuery("#db-list tr td a.rename").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var oldDatabase = PageContext.getAttribute("database");
        var newDatabase = PageContext.getAttribute("newDatabase");
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "oldDatabase=" + encodeURIComponent(oldDatabase);
        params[params.length] = "newDatabase=" + encodeURIComponent(newDatabase);
        alert("rename: " + params.join("&"));
    });

    jQuery("#alter-panel .panel-title span.close").click(function() {
        jQuery("#alter-panel").hide();
    });

    jQuery("#alter-panel input.cancel").click(function() {
        jQuery("#alter-panel").hide();
    });

    jQuery("#alter-panel input.ensure").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = jQuery(this).attr("database");

        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        AlterPanel.running();

        jQuery.ajax({
            "type": "post",
            "url": PageContext.getContextPath() + "/webcat/database/drop.html",
            "dataType": "json",
            "data": params.join("&"),
            "error": function() {
                AlterPanel.error("系统错误，请稍后再试！");
                AlterPanel.setEnabled(true);
            },
            "success": function(result) {
                if(result != null) {
                    jQuery("#alter-panel input.cancel").unbind();
                    jQuery("#alter-panel input.cancel").click(function() {
                        window.location.reload();
                    });
                    AlterPanel.show(result.message);
                }
                else {
                    AlterPanel.error("系统错误，请稍后再试！");
                    AlterPanel.setEnabled(true);
                }
            }
        });
    });

    jQuery("#db-list tr td a.remove").click(function() {
        var database = jQuery(this).attr("database");
        jQuery("#alter-panel input.ensure").attr("database", database);
        AlterPanel.submit(Sql.highlight("-- drop database\r\ndrop database " + database + ";"));
    });
});

jQuery(function() {
    Dragable.registe("db-panel");
    Dragable.registe("alter-panel");

    jQuery("#create-db-btn").click(function() {
        jQuery("#db-panel").show();
    });

    jQuery("#db-panel input.cancel").click(function() {
        jQuery("#db-panel").hide();
    });

    jQuery("#db-panel div.panel div.panel-title span.close").click(function() {
        jQuery("#db-panel").hide();
    });

    jQuery("#db-panel input.ensure").click(function() {
        var name = PageContext.getAttribute("connectionName");
        var database = jQuery("#db-panel input[name=database]").val();
        var charset = jQuery("#db-panel select[name=charset]").val();
        var collation = jQuery("#db-panel select[name=collation]").val();

        if(jQuery.trim(database).length < 1) {
            alert("数据库名称不能为空！");
            return;
        }

        var params = [];
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        params[params.length] = "charset=" + encodeURIComponent(charset);
        params[params.length] = "collation=" + encodeURIComponent(collation);

        jQuery.ajax({
            "type": "post",
            "url": PageContext.getContextPath() + "/webcat/database/create.html",
            "dataType": "json",
            "data": params.join("&"),
            "error": function() {
                alert("系统错误，请稍后再试！");
            },
            "success": function(result) {
                jQuery("#db-panel").hide();
                TextPanel.open(result.message);
                return;
            }
        });
    });
});

jQuery(function() {
    jQuery("#db-list tr").mouseover(function() {
        this.style.backgroundColor = "#ffda5a";
    });

    jQuery("#db-list tr").mouseout(function() {
        this.style.backgroundColor = "#ffffff";
    });
});

jQuery(function() {
    jQuery(window).trigger("resize");
});

jQuery(function() {
    var name = PageContext.getAttribute("connectionName");

    jQuery.ajax({
        "type": "post",
        "url": PageContext.getContextPath() + "/webcat/mysql/getCharsetList.html",
        "dataType": "json",
        "data": "name=" + name,
        "error": function() {
            // alert("系统错误，请稍后再试！");
        },
        "success": function(result) {
            if(result.status != 200) {
                return;
            }

            var charsetList = result.value;
            var e = jQuery("#db-panel select[name=charset]").get(0);

            for(var i = 0; i < charsetList.length; i++) {
                var charset = charsetList[i];
                var option = new Option(charset.charset + " -- " + charset.description, charset.charset);
                e.options.add(option);
            }
            e.value = "utf8";
            jQuery("#db-panel select[name=charset]").change();
        }
    });

    jQuery("#db-panel select[name=charset]").change(function() {
        var charset = this.value;
        var e = jQuery("#db-panel select[name=collation]").get(0);

        for(var i = e.length - 1; i > -1; i--) {
            e.options.remove(i);
        }

        if(charset.length < 1) {
            return;
        }

        jQuery.ajax({
            "type": "post",
            "url": PageContext.getContextPath() + "/webcat/mysql/getCollationList.html",
            "dataType": "json",
            "data": "name=" + name + "&charset=" + charset,
            "error": function() {
                // alert("系统错误，请稍后再试！");
            },
            "success": function(result) {
                if(result.status != 200) {
                    return;
                }

                var collationList = result.value;

                for(var i = 0; i < collationList.length; i++) {
                    var collation = collationList[i];
                    var option = new Option(collation.collation, collation.collation);
                    e.options.add(option);
                }
            }
        });
    });
});
