jQuery(function() {
    var container = jQuery(document.body).children("div.tab-component").get(0);
    new TabPanel({"container": container});
});

jQuery(function() {
    var getTableName = function(result) {
        var table = document.getElementById("columnList");

        if(table == null) {
            return;
        }
        return table.getAttribute("tableName");
    };

    var getTypeName = function(javaTypeName) {
        var a = javaTypeName;

        if(a == "boolean" || a == "byte" || a == "short" || a == "int" || a == "float" || a == "double"|| a == "long") {
            return "number";
        }

        if(a == "char" || a == "String") {
            return "string";
        }

        if(a == "java.util.Date" || a == "java.sql.Date" || a == "java.sql.Timestamp") {
            return "datatime";
        }
        return "unknown";
    }

    var getOptionsHtml = function(name, dataType) {
        var b = [];
        var t = ["unknown", "number", "string", "date", "datatime"];
        b[b.length] = "<select name=\"" + name + "\">";

        for(var i = 0; i < t.length; i++) {
            if(t[i] == dataType) {
                b[b.length] = "<option value=\"" + t[i] + "\" selected=\"true\">" + t[i] + "</option>";
            }
            else {
                b[b.length] = "<option value=\"" + t[i] + "\">" + t[i] + "</option>";
            }
        }
        return b.join("");
    };

    var showColumnList = function(result) {
        var table = document.getElementById("columnList");

        if(table == null) {
            return;
        }
        var rows = table.rows;
        var size = rows.length;

        for(var i = size - 1; i > -1; i--) {
            var e = rows[i];
            e.parentNode.removeChild(e);
        }

        var list = result.columns;
        var tableName = result.tableName;
        table.setAttribute("tableName", tableName);

        for(var i = 0; i < list.length; i++) {
            var column = list[i];
            var tr = table.insertRow(-1);
            var td1 = tr.insertCell(-1);
            var td2 = tr.insertCell(-1);
            var td3 = tr.insertCell(-1);
            var td4 = tr.insertCell(-1);
            var td5 = tr.insertCell(-1);
            var fieldName = column.variableName;
            var columnName = column.columnName;
            var columnValue = column.columnValue;
            var columnType = getTypeName(column.javaTypeName);
            var typeName = "data-type-string";

            if(columnType == "number") {
                typeName = "data-type-int";
            }
            else if (columnType == "string"){
                typeName = "data-type-string";
            }
            else if (columnType == "date"){
                typeName = "data-type-date";
            }
            else if (columnType == "datatime"){
                typeName = "data-type-date";
            }
            else{
                typeName = "data-type-unknown";
            }

            td1.className = "w30";
            td2.className = "w200";
            td3.className = "w100";
            td4.className = "w500";
            td5.className = "";

            td1.innerHTML = i + 1;
            td2.innerHTML = "<input class=\"" + typeName + "\" readonly=\"true\" value=\"" + columnName + "\"/>";
            td3.innerHTML = getOptionsHtml("dataType", columnType);
            td4.innerHTML = "<input name=\"" + columnName + "\" type=\"text\" class=\"column-value\" style=\"width: 470px;\" value=\"" + HtmlUtil.encode(columnValue, "\r\n") + "\"/><span name=\"edit\" class=\"edit\"></span>";
            td5.innerHTML = "&nbsp;<a class=\"delete\" href=\"javascript:void(0)\">删除列</a>";

            tr.setAttribute("fieldName", fieldName);
            tr.setAttribute("columnName", columnName);
            tr.setAttribute("columnType", columnType);
            tr.setAttribute("columnValue", (columnValue || ""));
        }
    };

    var setColumnValue = function(name, value) {
        var table = document.getElementById("columnList");

        if(table != null) {
            var rows = table.rows;
            var size = rows.length;

            for(var i = 0; i < size; i++) {
                var e = rows[i];

                if(e.getAttribute("columnName") == name) {
                    e.setAttribute("columnValue", value);
                    jQuery(e).find("td input[name=" + name + "]").val(value);
                    break;
                }
            }
        }
    };

    var resetOrderNum = function() {
        var table = document.getElementById("columnList");

        if(table != null) {
            var rows = table.rows;
            var size = rows.length;

            for(var i = 0; i < size; i++) {
                var e = rows[i];
                e.cells[0].innerHTML = (e.rowIndex + 1);
            }
        }
    };

    var showEditPanel = function(column) {
        jQuery("#edit-panel").show();
        jQuery("#edit-panel div.panel-title h4").html("<span class=\"icon-table\"></span>编辑字段[" + column.name + "]");
        jQuery("#edit-panel").attr("columnName", column.name);
        jQuery("#edit-panel textarea[name=content]").val(column.value);
        jQuery("#edit-panel textarea[name=content]").select();
    };

    var getRecord = function() {
        var record = [];
        var table = document.getElementById("columnList");

        if(table != null) {
            var rows = table.rows;
            var size = rows.length;

            for(var i = 0; i < size; i++) {
                var e = rows[i];
                var fieldName = e.getAttribute("fieldName");
                var columnName = e.getAttribute("columnName");
                var columnType = e.getAttribute("columnType");
                var columnValue = e.getAttribute("columnValue");
                record[record.length] = {"fieldName": fieldName, "columnName": columnName, "columnType": columnType, "columnValue": columnValue};
            }
        }
        return record;
    };

    var sqlEscape = function(source) {
        if(source == null) {
            return "";
        }
        var buffer = [];

        for(var i = 0, size = source.length; i < size; i++) {
            c = source.charAt(i);

            switch (c) {
                case '\\': {
                    buffer[buffer.length] = "\\\\"; break;
                }
                case '\'': {
                    buffer[buffer.length] = "\\\'"; break;
                }
                case '"': {
                    buffer[buffer.length] = "\\\""; break;
                }
                case '\r': {
                    buffer[buffer.length] = "\\r"; break;
                }
                case '\n': {
                    buffer[buffer.length] = "\\n"; break;
                }
                case '\t': {
                    buffer[buffer.length] = "\\t"; break;
                }
                case '\b': {
                    buffer[buffer.length] = "\\b"; break;
                }
                case '\f': {
                    buffer[buffer.length] = "\\f"; break;
                }
                default:  {
                    buffer[buffer.length] = c; break;
                }
            }   
        }
        return buffer.join("");
    };

    var getInsertSql = function() {
        var record = getRecord();
        var buffer = ["insert into "];
        buffer[buffer.length] = getTableName() + "(";

        for(var i = 0; i < record.length; i++) {
            var column = record[i];
            buffer[buffer.length] = column.columnName;

            if(i + 1 < record.length) {
                buffer[buffer.length] = ", ";
            }
        }
        buffer[buffer.length] = ") values (";

        for(var i = 0; i < record.length; i++) {
            var column = record[i];
            var columnType = column.columnType;
            var columnValue = column.columnValue;

            if(columnType == "unknown") {
                columnType = "string";
            }

            if(columnType == "number") {
                if(columnValue.length < 1 || isNaN(columnValue)) {
                    buffer[buffer.length] = "null";
                }
                else {
                    buffer[buffer.length] = columnValue;
                }
            }
            else if(columnType == "string") {
                buffer[buffer.length] = "'" + sqlEscape(columnValue) + "'";
            }
            else{
                buffer[buffer.length] = "'" + sqlEscape(columnValue) + "'";
            }

            if(i + 1 < record.length) {
                buffer[buffer.length] = ", ";
            }
        }
        buffer[buffer.length] = ");";
        return buffer.join("");
    };

    var getJsonString = function() {
        var record = getRecord();
        var buffer = [];

        for(var i = 0; i < record.length; i++) {
            var column = record[i];
            var fieldName = column.fieldName;
            var columnType = column.columnType;
            var columnValue = column.columnValue;

            if(columnType == "unknown") {
                columnType = "string";
            }

            if(columnType == "number") {
                if(columnValue.length < 1 || isNaN(columnValue)) {
                    buffer[buffer.length] = "\"" + fieldName + "\":null";
                }
                else {
                    buffer[buffer.length] = "\"" + fieldName + "\":" + columnValue;
                }
            }
            else if(columnType == "string") {
                buffer[buffer.length] = "\"" + fieldName + "\":\"" + sqlEscape(columnValue) + "\"";
            }
            else{
                buffer[buffer.length] = "\"" + fieldName + "\":\"" + sqlEscape(columnValue) + "\"";
            }
        }
        return "{" + buffer.join(",") + "}";
    };

    var showResult = function() {
        var type = jQuery("input[name=resultType]:checked").val();

        if(type == "json") {
            jQuery("#result").val(getJsonString());
        }
        else {
            jQuery("#result").val(getInsertSql());
        }
    };

    SimpleDrag.register("editPanelTitle", "editPanel");

    jQuery("#source").change(function() {
        jQuery("#parseBtn").click();
    });

    jQuery("#columnList select[name=dataType]").live("change", function() {
        var tr = jQuery(this).closest("tr");
        tr.attr("columnType", this.value);
    });

    jQuery("#columnList input.column-value").live("change", function() {
        var tr = jQuery(this).closest("tr");
        var columnType = tr.attr("columnType");

        if(columnType == "number" && isNaN(this.value)) {
            this.value = "0";
            tr.attr("columnValue", "0");
        }
        else {
            tr.attr("columnValue", this.value);
        }
        showResult();
    });

    jQuery("span.edit").live("click", function() {
        var tr = jQuery(this).closest("tr");
        var name = tr.attr("columnName");
        var value = tr.attr("columnValue");
        showEditPanel({"name": name, "value": value});
    });

    jQuery("a.delete").live("click", function() {
        jQuery(this).closest("tr").remove();
        resetOrderNum();
        showResult();
    });

    jQuery("#edit-panel span.close").live("click", function() {
        jQuery("#edit-panel").hide();
    });

    jQuery("#edit-panel input[name=ensure]").live("click", function() {
        var name = jQuery("#edit-panel").attr("columnName");
        var value = jQuery("#edit-panel textarea[name=content]").val();
        setColumnValue(name, value);
        showResult();
        jQuery("#edit-panel").hide();
    });

    jQuery("#edit-panel input[name=cancel]").live("click", function() {
        jQuery("#edit-panel").hide();
    });

    jQuery("#toolsBtn").click(function() {
        window.location.href = "/tools.html";
    });

    jQuery("#parseBtn").click(function() {
        var sql = jQuery.trim(jQuery("#source").val());

        if(sql.length < 1) {
            return;
        }

        jQuery.ajax({
            "type": "post",
            "url": "/webcat/tools/sql/create/parse.html",
            "data": {"sql": sql},
            "dataType": "json",
            "error": function() {
                alert("解析失败！");
            },
            "success": function(returnValue) {
                Response.success(returnValue, function(result) {
                    showColumnList(result);
                });
            }
        });
    });

    jQuery("#insertBtn").click(function() {
        showResult();
    });

    jQuery(window).bind("resize", function() {
        var e = document.getElementById("columnPanel");

        if(e != null) {
            var offset = parseInt(e.getAttribute("offset-top"));

            if(isNaN(offset)) {
                offset = 280;
            }

            var height = document.documentElement.clientHeight - offset;
            e.style.height = height + "px";
        }
    });
    Dragable.registe("edit-panel");
    jQuery(window).trigger("resize");
});
