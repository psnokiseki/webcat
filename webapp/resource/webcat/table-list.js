jQuery(function() {
    var tabPanel = new SqlPlusPanel({"container": "table-list-panel"});

    jQuery("#table-list tr td a.query").click(function() {
        tabPanel.add("select * from " + this.getAttribute("tableName") + ";\r\n\r\n\r\n\r\n\r\n\r\n");
    });

    jQuery("#table-list tr td a.rename").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        var tableName = jQuery(this).attr("tableName");
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        params[params.length] = "tableName=" + encodeURIComponent(tableName);
        alert("rename: " + params.join("&"));
    });

    jQuery("#table-list tr td a.edit").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        var tableName = jQuery(this).attr("tableName");
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        params[params.length] = "tableName=" + encodeURIComponent(tableName);
        window.location.href = PageContext.getContextPath() + "/webcat/table/edit.html?" + params.join("&");
    });

    jQuery("#alter-panel .panel-title span.close").click(function() {
        jQuery("#alter-panel").hide();
    });

    jQuery("#alter-panel input.cancel").click(function() {
        jQuery("#alter-panel").hide();
    });

    jQuery("#alter-panel input.ensure").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        var tableName = jQuery(this).attr("tableName");

        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        params[params.length] = "tableName=" + encodeURIComponent(tableName);
        AlterPanel.running();

        jQuery.ajax({
            "type": "post",
            "url": PageContext.getContextPath() + "/webcat/table/drop.html",
            "dataType": "json",
            "data": params.join("&"),
            "error": function() {
                AlterPanel.error("系统错误，请稍后再试！");
                AlterPanel.setEnabled(true);
            },
            "success": function(result) {
                if(result != null) {
                    jQuery("#alter-panel input.cancel").unbind();
                    jQuery("#alter-panel input.cancel").click(function() {
                        window.location.reload();
                    });
                    AlterPanel.show(result.message);
                }
                else {
                    AlterPanel.error("系统错误，请稍后再试！");
                    AlterPanel.setEnabled(true);
                }
            }
        });
    });

    jQuery("#table-list tr td a.delete").click(function() {
        var tableName = jQuery(this).attr("tableName");
        jQuery("#alter-panel input.ensure").attr("tableName", tableName);
        AlterPanel.submit(Sql.highlight("-- drop table\r\ndrop table " + tableName + ";"));
    });

    jQuery("#table-list tr td a.insert").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        var tableName = jQuery(this).attr("tableName");
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        params[params.length] = "tableName=" + encodeURIComponent(tableName);
        window.location.href = PageContext.getContextPath() + "/webcat/table/insertEdit.html?" + params.join("&");
    });

    jQuery("#table-list tr td a.generate").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        var tableName = jQuery(this).attr("tableName");
        params[params.length] = "connectionName=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        params[params.length] = "tableName=" + encodeURIComponent(tableName);
        window.location.href = PageContext.getContextPath() + "/generate/table/edit.html?" + params.join("&");
    });
});

jQuery(function() {
    jQuery("#create-table-btn").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        window.location.href = PageContext.getContextPath() + "/webcat/table/create.html?" + params.join("&");
    });

    jQuery("#import-table-btn").click(function() {
        // alert("该功能尚未实现！");
    });

    jQuery("#export-table-btn").click(function() {
        // alert("该功能尚未实现！");
    });

    /**
     * 未实现的功能暂不显示
     */
    jQuery("#import-table-btn").remove();
    jQuery("#export-table-btn").remove();

    jQuery("#backup-table-btn").click(function() {
        var params = [];
        var name = PageContext.getAttribute("connectionName");
        var database = PageContext.getAttribute("database");
        params[params.length] = "name=" + encodeURIComponent(name);
        params[params.length] = "database=" + encodeURIComponent(database);
        window.location.href = PageContext.getContextPath() + "/webcat/backup/index.html?" + params.join("&");
    });
});

jQuery(function() {
    jQuery("#table-list tr").mouseover(function() {
        this.style.backgroundColor = "#ffda5a";
    });

    jQuery("#table-list tr").mouseout(function() {
        this.style.backgroundColor = "#ffffff";
    });
});

jQuery(function() {
    jQuery(window).resize();
});
