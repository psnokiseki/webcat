/*
 * $RCSfile: webcat.js,v $$
 * $Revision: 1.1 $
 * $Date: 2012-10-18 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 * @author xuesong.net
 */
var DispatchAction = {"listeners": []};

DispatchAction.dispatch = function(pattern, action) {
    var listeners = this.listeners;
    var object = {"pattern": pattern, "action": action};
    listeners[listeners.length] = object;
    return object;
};

DispatchAction.call = function(hash, rule) {
    var pattern = rule.pattern;
    var action = rule.action;

    if(pattern == "*") {
        action.apply(null, []);
        return true;
    }

    var regExp = new RegExp(pattern);
    var params = regExp.exec(hash);

    if(params != null) {
        var args = [];

        for (var i = 1; i < params.length; i++) {
            args[args.length] = params[i];
        }

        action.apply(null, args);
        return true;
    }
    return false;
};

DispatchAction.execute = function(hash) {
    var hash = (hash != null ? hash: window.location.hash);

    if(hash == null || hash == undefined) {
        hash = "";
    }
    else {
        hash = hash.replace(/(^\s*)|(\s*$)/g, "");
    }

    if(hash.charAt(0) == "#") {
        hash = hash.substring(1);
    }

    var listeners = this.listeners;

    for (var i = 0, length = listeners.length; i < length; i++) {
        DispatchAction.call(hash, listeners[i]);
    }
};

var CookieUtil = {};
CookieUtil.trim = function(s){return (s != null ? s.replace(/(^\s*)|(\s*$)/g, "") : "");};
CookieUtil.nval = function() {
    var i = 0;
    var e = null;

    for(i = 0; i < arguments.length; i++) {
        e = arguments[i];

        if(e == null || e == undefined) {
            continue;
        }

        if(typeof(e) == "string") {
            e = CookieUtil.trim(e);

            if(e.length > 0) {
                return e;
            }
        }
        else {
            return e;
        }
    }
    return null;
};
CookieUtil.setCookie = function(cookie) {
    var expires = "";
    if(cookie.value == null) {
        cookie.value = "";
        cookie.expires = -1;
    }

    if(cookie.expires != null) {
        var date = null;

        if(typeof(cookie.expires) == "number") {
            date = new Date();
            date.setTime(date.getTime() + cookie.expires * 1000);
        }
        else if(cookie.expires.toUTCString != null) {
            date = cookie.expires;
        }

        // use expires attribute, max-age is not supported by IE
        if(date != null) {
            expires = "; expires=" + date.toUTCString();
        }
    }
    var path = cookie.path ? "; path=" + (cookie.path) : "";
    var domain = cookie.domain ? "; domain=" + (cookie.domain) : "";
    var secure = cookie.secure ? "; secure" : "";
    document.cookie = [cookie.name, "=", (cookie.value != null ? encodeURIComponent(cookie.value) : ""), expires, path, domain, secure].join("");
};

CookieUtil.getCookie = function(name, defaultValue) {
    var value = null;

    if(document.cookie && document.cookie != "") {
        var cookies = document.cookie.split(';');

        for(var i = 0; i < cookies.length; i++) {
            var cookie = CookieUtil.trim(cookies[i]);

            if(cookie.substring(0, name.length + 1) == (name + "=")) {
                value = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return CookieUtil.nval(value, defaultValue);
};

CookieUtil.remove = function(name, path) {
    CookieUtil.setCookie({"name": name, "path": path});
};

(function() {
    var reload = CookieUtil.getCookie("reload");

    if(reload == "true") {
        CookieUtil.remove("reload", "/");
        window.location.reload();
    }
})();

var Response = {};

Response.error = function(result) {
    if(result != null && result.message != null) {
        alert(result.message);
    }
    else {
        alert("系统错误，请稍后再试！");
    }
};

Response.success = function(result, success) {
    if(result == null){
        alert("系统错误，请稍后再试！");
        return false;
    }

    if(result.code == 0 || result.status == 200) {
        if(success != null){
            success(result.value);
        }
        return true;
    }

    if(result.message != null){
        alert(result.message);
    }
    else{
        alert("系统错误，请稍后再试！");
    }
    return false;
};

var PageContext = {};

PageContext.getContextPath = function(){
    if(this.contextPath != null){
        return this.contextPath;
    }

    var contextPath = this.getAttribute("contextPath");

    if(contextPath == null || contextPath == "/"){
        contextPath = "";
    }
    return (this.contextPath = contextPath);
};

PageContext.setAttribute = function(name, value){
    var e = document.getElementById("pageContext");

    if(e != null) {
        e.setAttribute(name, value);
    }
};

PageContext.getAttribute = function(name, defaultValue){
    var value = null;
    var e = document.getElementById("pageContext");

    if(e != null) {
        value = e.getAttribute(name);
    }

    if(value != null && value != undefined) {
        return value;
    }

    if(defaultValue != null && defaultValue != undefined) {
        return defaultValue;
    }
    return null;
};

PageContext.getObject = function(name){
    var data = this.getAttribute(name);

    if(data == null){
        return null;
    }

    try {
        return window.eval("(" + data + ")");
    }
    catch(e) {
    }
    return null;
};

PageContext.getInt = function(name){
    var value = this.getAttribute(name);
    if(value == null){
        return 0;
    }
    value = parseInt(value);
    return (isNaN(value) ? 0 : value);
};

PageContext.getLong = function(name){
    var value = this.getAttribute(name);
    if(value == null){
        return 0;
    }
    value = parseInt(value);
    return (isNaN(value) ? 0 : value);
};

PageContext.back = function() {
    CookieUtil.setCookie({"name": "reload", "value": "true", "path": "/"});
    window.history.back();
};

var HtmlUtil = {};
HtmlUtil.replacement = {
    "<": "&lt;",
    ">": "&gt;",
    "\"": "&quot;",
    " ": "&nbsp;",
    "\u00ae": "&reg;",
    "\u00a9": "&copy;"
};

HtmlUtil.replace = function(e){return HtmlUtil.replacement[e];};

HtmlUtil.encode = function(source, crlf) {
    if(source == null) {
        return "";
    }

    if(crlf == null) {
        crlf = "<br/>";
    }

    if(typeof(source) != "string") {
        source = source.toString();
    }
    return source.replace(new RegExp("[<>\"\\u00ae\\u00a9]", "g"), HtmlUtil.replace).replace(new RegExp("\\r?\\n", "g"), crlf);
};
