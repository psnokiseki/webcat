var UnsupportOperatorError = new Error("UnsupportOperator", "unsupport operator error!");

var DataType = function() {
    this.config = {};
};

/**
 * @param jdbcType
 * @param javaType
 */
DataType.prototype.add = function(jdbcType, javaType) {
    this.config[jdbcType.toLowerCase()] = javaType;
};

/**
 * @param jdbcType
 * @return String
 */
DataType.prototype.getJavaType = function(jdbcType) {
    var javaType = this.config[jdbcType.toLowerCase()];

    if(javaType == null || javaType == undefined) {
        return "String";
    }
    else {
        return javaType;
    }
};

DataType.prototype.getPrimitiveType = function(type) {
    if(type == "Character" || type == "java.lang.Character") {
        return "char";
    }
    else if(type == "Boolean" || type == "java.lang.Boolean") {
        return "boolean";
    }
    else if(type == "Byte" || type == "java.lang.Byte") {
        return "byte";
    }
    else if(type == "Short" || type == "java.lang.Short") {
        return "short";
    }
    else if(type == "Integer" || type == "java.lang.Integer") {
        return "int";
    }
    else if(type == "Float" || type == "java.lang.Float") {
        return "float";
    }
    else if(type == "Double" || type == "java.lang.Double") {
        return "double";
    }
    else if(type == "Long" || type == "java.lang.Long") {
        return "long";
    }
    return type;
};

DataType.prototype.getWrapperType = function(type) {
    if(type == "char") {
        return "Character";
    }
    else if(type == "boolean") {
        return "Boolean";
    }
    else if(type == "byte") {
        return "Byte";
    }
    else if(type == "short") {
        return "Short";
    }
    else if(type == "int") {
        return "Integer";
    }
    else if(type == "float") {
        return "Float";
    }
    else if(type == "double") {
        return "Double";
    }
    else if(type == "long") {
        return "Long";
    }
    return type;
};

/**
 * @interface
 */
DataType.prototype.getJdbcType = function(javaType){
    throw UnsupportOperatorError;
};

function generate() {
    var b = [];
    var a = ["BIGINT UNSIGNED", "BIGINT", "INT UNSIGNED", "INT",
        "MEDIUMINT UNSIGNED", "MEDIUMINT", "SMALLINT UNSIGNED",
        "SMALLINT", "TINYINT UNSIGNED", "TINYINT", "BOOLEAN",
        "FLOAT", "DOUBLE", "DECIMAL", "CHAR", "VARCHAR","BINARY",
        "VARBINARY", "TEXT", "MEDIUMTEXT", "LONGTEXT", "DATE",
        "DATETIME", "TIMESTAMP", "TIME", "YEAR", "CLOB", "BLOB", "LONGBLOB"];

    var padding = function(source, length, pad) {
        var buffer = [source];
        var count = source.length;

        while(count < length) {
            buffer[buffer.length] = pad;
            count += pad.length;
        }

        var result = buffer.join("");

        if(result.length > length) {
            return result.substring(0, length);
        }
        return result;
    }

    var maxLength = 0;

    for(var i = 0; i < a.length; i++) {
        if(a[i].length > maxLength) {
            maxLength = a[i].length;
        }
    }

    maxLength = maxLength + 3;

    for(var i = 0; i < a.length; i++) {
        b[b.length] = "MySqlDataType.add(" + padding("\"" + a[i] + "\",", maxLength, " ") + " \"Long\");";
    }
    console.log(b.join("\r\n"));
    return b.join("\r\n");
}

var MySqlDataType = new DataType();

MySqlDataType.getJdbcType = function(javaType){
    if(javaType == "String") {
        return "VARCHAR(32)";
    }
    else if(javaType == "int" || javaType == "Integer") {
        return "INTEGER";
    }
    else if(javaType == "float" || javaType == "Float") {
        return "INTEGER";
    }
    else if(javaType == "double" || javaType == "Double") {
        return "DOUBLE";
    }
    else if(javaType == "long" || javaType == "Long") {
        return "LONG";
    }
    else if(javaType == "Date") {
        return "DATE|DATETIME|TIMESTAMP";
    }
    else if(javaType == "Timestamp") {
        return "TIMESTAMP";
    }
    else if(javaType == "Reader") {
        return "CLOB";
    }
    else if(javaType == "InputStream") {
        return "BLOB";
    }
    else if(javaType == "Clob") {
        return "TEXT|MEMO|CLOB";
    }
    else if(javaType == "Blob") {
        return "BLOB";
    }
    else {
        return "UNKNOWN(32)";
    }
};

MySqlDataType.add("BIGINT UNSIGNED",    "Long");
MySqlDataType.add("BIGINT",             "Long");
MySqlDataType.add("INT UNSIGNED",       "Long");
MySqlDataType.add("INT",                "int");
MySqlDataType.add("MEDIUMINT UNSIGNED", "int");
MySqlDataType.add("MEDIUMINT",          "int");
MySqlDataType.add("SMALLINT UNSIGNED",  "int");
MySqlDataType.add("SMALLINT",           "int");
MySqlDataType.add("TINYINT UNSIGNED",   "int");
MySqlDataType.add("TINYINT",            "int");
MySqlDataType.add("BOOLEAN",            "int");
MySqlDataType.add("FLOAT",              "float");
MySqlDataType.add("DOUBLE",             "double");
MySqlDataType.add("DECIMAL",            "Long");
MySqlDataType.add("CHAR",               "String");
MySqlDataType.add("VARCHAR",            "String");
MySqlDataType.add("BINARY",             "String");
MySqlDataType.add("VARBINARY",          "String");
MySqlDataType.add("TEXT",               "String");
MySqlDataType.add("MEDIUMTEXT",         "String");
MySqlDataType.add("LONGTEXT",           "String");
MySqlDataType.add("DATE",               "java.util.Date");
MySqlDataType.add("DATETIME",           "java.util.Date");
MySqlDataType.add("TIMESTAMP",          "java.util.Date");
MySqlDataType.add("TIME",               "java.util.Date");
MySqlDataType.add("YEAR",               "int");
MySqlDataType.add("CLOB",               "Reader");
MySqlDataType.add("BLOB",               "InputStream");
MySqlDataType.add("LONGBLOB",           "InputStream");
