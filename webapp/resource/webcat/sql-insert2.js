jQuery(function() {
    var container = jQuery(document.body).children("div.tab-component").get(0);
    new TabPanel({"container": container});
});

jQuery(function() {
    var getTableName = function(result) {
        var table = document.getElementById("columnList");

        if(table == null) {
            return;
        }
        return table.getAttribute("tableName");
    };

    var getTypeName = function(javaTypeName) {
        var a = javaTypeName;

        if(a == "boolean" || a == "byte" || a == "short" || a == "int" || a == "float" || a == "double"|| a == "long") {
            return "number";
        }

        if(a == "char" || a == "String") {
            return "string";
        }

        if(a == "java.util.Date" || a == "java.sql.Date" || a == "java.sql.Timestamp") {
            return "datetime";
        }
        return "unknown";
    }

    var getOptionsHtml = function(name, defaultValue) {
        var b = [];
        var t = ["unknown", "number", "string", "datetime"];
        b[b.length] = "<select name=\"" + name + "\">";

        for(var i = 0; i < t.length; i++) {
            if(t[i] == defaultValue) {
                b[b.length] = "<option value=\"" + t[i] + "\" selected=\"true\">" + t[i] + "</option>";
            }
            else {
                b[b.length] = "<option value=\"" + t[i] + "\">" + t[i] + "</option>";
            }
        }
        return b.join("");
    };

    var showColumnList = function(result) {
        var table = document.getElementById("columnList");

        if(table == null) {
            return;
        }
        var rows = table.rows;
        var size = rows.length;

        for(var i = size - 1; i > -1; i--) {
            var e = rows[i];
            e.parentNode.removeChild(e);
        }

        var list = result.columns;
        var tableName = result.tableName;
        table.setAttribute("tableName", tableName);

        for(var i = 0; i < list.length; i++) {
            var column = list[i];
            var tr = table.insertRow(-1);
            var td1 = tr.insertCell(-1);
            var td2 = tr.insertCell(-1);
            var td3 = tr.insertCell(-1);
            var td4 = tr.insertCell(-1);
            var td5 = tr.insertCell(-1);
            var td6 = tr.insertCell(-1);
            var columnName = column.columnName;
            var columnValue = "";
            var columnType = getTypeName(column.javaTypeName);
            var nullable = column.nullable;
            var typeName = "data-type-string";

            if(columnType == "number") {
                typeName = "data-type-int";
            }
            else if (columnType == "string"){
                typeName = "data-type-string";
            }
            else if (columnType == "datetime"){
                typeName = "data-type-date";
            }
            else{
                typeName = "data-type-unknown";
            }

            td1.className = "w30";
            td2.className = "w200";
            td3.className = "w100";
            td4.className = "w100 center";
            td5.className = "w300";
            td6.className = "";

            td1.innerHTML = i + 1;
            td2.innerHTML = "<input name=\"webcat-column-name\" type=\"text\" class=\"" + typeName + "\" readonly=\"true\" value=\"" + columnName + "\"/>";
            td3.innerHTML = getOptionsHtml("webcat-column-type", columnType);

            if(nullable == 1) {
                td4.innerHTML = "<input type=\"checkbox\" disabled=\"true\"/>";
            }
            else {
                td4.innerHTML = "<input type=\"checkbox\" disabled=\"true\" checked=\"true\"/>";
            }
            td5.innerHTML = "<input name=\"" + columnName + "\" type=\"text\" class=\"column-value\" value=\"" + HtmlUtil.encode(columnValue, "\r\n") + "\"/><span name=\"edit\" class=\"edit\"></span>";
            td6.innerHTML = "&nbsp;<a class=\"delete\" href=\"javascript:void(0)\">删除列</a>";

            tr.setAttribute("columnType", columnType);
            tr.setAttribute("columnName", columnName);
            tr.setAttribute("columnValue", columnValue);
            tr.setAttribute("nullable", nullable);
        }
    };

    var setColumnValue = function(name, value) {
        var table = document.getElementById("columnList");

        if(table != null) {
            var rows = table.rows;
            var size = rows.length;

            for(var i = 0; i < size; i++) {
                var e = rows[i];

                if(e.getAttribute("columnName") == name) {
                    e.setAttribute("columnValue", value);
                    jQuery(e).find("td input[name=" + name + "]").val(value);
                    break;
                }
            }
        }
    };

    var resetOrderNum = function() {
        var table = document.getElementById("columnList");

        if(table != null) {
            var rows = table.rows;
            var size = rows.length;

            for(var i = 0; i < size; i++) {
                var e = rows[i];
                e.cells[0].innerHTML = (e.rowIndex + 1);
            }
        }
    };

    var showEditPanel = function(column) {
        jQuery("#edit-panel").show();
        jQuery("#edit-panel div.panel-title h4").html("<span class=\"icon-table\"></span>编辑字段[" + column.name + "]");
        jQuery("#edit-panel").attr("columnName", column.name);
        jQuery("#edit-panel textarea[name=content]").val(column.value);
        jQuery("#edit-panel textarea[name=content]").select();
    };

    var getRecord = function() {
        var record = [];
        var table = document.getElementById("columnList");

        if(table != null) {
            var rows = table.rows;
            var size = rows.length;

            for(var i = 0; i < size; i++) {
                var e = rows[i];
                var columnType = e.getAttribute("columnType");
                var columnName = e.getAttribute("columnName");
                var columnValue = e.getAttribute("columnValue");
                var nullable = e.getAttribute("nullable");
                record[record.length] = {"columnType": columnType, "columnName": columnName, "columnValue": columnValue, "nullable": nullable};
            }
        }
        return record;
    };

    var sqlEscape = function(source) {
        if(source == null) {
            return "";
        }
        var buffer = [];

        for(var i = 0, size = source.length; i < size; i++) {
            c = source.charAt(i);

            switch (c) {
                case '\\': {
                    buffer[buffer.length] = "\\\\"; break;
                }
                case '\'': {
                    buffer[buffer.length] = "\\\'"; break;
                }
                case '"': {
                    buffer[buffer.length] = "\\\""; break;
                }
                case '\r': {
                    buffer[buffer.length] = "\\r"; break;
                }
                case '\n': {
                    buffer[buffer.length] = "\\n"; break;
                }
                case '\t': {
                    buffer[buffer.length] = "\\t"; break;
                }
                case '\b': {
                    buffer[buffer.length] = "\\b"; break;
                }
                case '\f': {
                    buffer[buffer.length] = "\\f"; break;
                }
                default:  {
                    buffer[buffer.length] = c; break;
                }
            }   
        }
        return buffer.join("");
    };

    var getInsertSql = function() {
        var record = getRecord();
        var buffer = ["insert into "];
        buffer[buffer.length] = getTableName() + "(";

        for(var i = 0; i < record.length; i++) {
            var column = record[i];
            buffer[buffer.length] = column.columnName;

            if(i + 1 < record.length) {
                buffer[buffer.length] = ", ";
            }
        }
        buffer[buffer.length] = ") values (";

        for(var i = 0; i < record.length; i++) {
            var column = record[i];
            var columnType = column.columnType;
            var columnValue = column.columnValue;

            if(columnType == "unknown") {
                columnType = "string";
            }

            if(columnType == "number") {
                if(isNaN(columnValue) || columnValue.length < 1) {
                    buffer[buffer.length] = "null";
                }
                else {
                    buffer[buffer.length] = columnValue;
                }
            }
            else if(columnType == "string") {
                buffer[buffer.length] = "'" + sqlEscape(columnValue) + "'";
            }
            else{
                buffer[buffer.length] = "'" + sqlEscape(columnValue) + "'";
            }

            if(i + 1 < record.length) {
                buffer[buffer.length] = ", ";
            }
        }
        buffer[buffer.length] = ");";
        return buffer.join("");
    };

    jQuery("#columnList select[name=webcat-column-type]").live("change", function() {
        var dataType = this.value;
        var tr = jQuery(this).closest("tr");
        tr.attr("columnType", this.value);
        tr.find("input[name=webcat-column-name]").attr("class", "data-type-" + dataType);
    });

    jQuery("#columnList input.column-value").live("change", function() {
        var tr = jQuery(this).closest("tr");
        var columnType = tr.attr("columnType");

        if(columnType == "number" && isNaN(this.value)) {
            this.value = "0";
            tr.attr("columnValue", "0");
        }
        else {
            tr.attr("columnValue", this.value);
        }
        jQuery("#result").val(getInsertSql());
    });

    jQuery("span.edit").live("click", function() {
        var tr = jQuery(this).closest("tr");
        var name = tr.attr("columnName");
        var value = tr.attr("columnValue");
        showEditPanel({"name": name, "value": value});
    });

    jQuery("a.delete").live("click", function() {
        jQuery(this).closest("tr").remove();
        resetOrderNum();
        jQuery("#result").val(getInsertSql());
    });

    jQuery("#edit-panel span.close").live("click", function() {
        jQuery("#edit-panel").hide();
    });

    jQuery("#edit-panel input[name=ensure]").click(function() {
        var name = jQuery("#edit-panel").attr("columnName");
        var value = jQuery("#edit-panel textarea[name=content]").val();
        setColumnValue(name, value);
        jQuery("#result").val(getInsertSql());
        jQuery("#edit-panel").hide();
    });

    jQuery("#edit-panel input[name=cancel]").click(function() {
        jQuery("#edit-panel").hide();
    });

    jQuery("#tools-box").click(function() {
        window.location.href = "/tools.html";
    });

    jQuery("#insert-btn").click(function() {
        jQuery("#result").val(getInsertSql());
    });

    jQuery(window).bind("resize", function() {
        var e = document.getElementById("columnPanel");

        if(e != null) {
            var offset = parseInt(e.getAttribute("offset-top"));

            if(isNaN(offset)) {
                offset = 164;
            }

            var height = document.documentElement.clientHeight - offset;
            e.style.height = height + "px";
        }
    });

    Dragable.registe("edit-panel");

    if(table != null) {
        showColumnList(table);
        jQuery("#result").val(getInsertSql());
    }
    jQuery(window).trigger("resize");
});