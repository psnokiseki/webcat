/*
 * $RCSfile: Frame.js,v $$
 * $Revision: 1.1 $
 * $Date: 2015-11-22 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
var App = {};
App.setViewType = function(flag) {
    var f1 = document.getElementById("leftPanel");
    var f2 = document.getElementById("ctrlPanel");
    var f3 = document.getElementById("mainPanel");
    var f4 = document.getElementById("ctrlBtn");
    var viewType = parseInt(flag);

    if(viewType == 1) {
        /* [200, 0, *] */
        f1.parentNode.style.display = "block";
        f1.parentNode.style.width = "240px";
        f2.style.marginLeft = "0px";
        f2.style.display = "none";
        f1.style.display = "block";
        f3.style.marginLeft = "240px";
    }
    else if(viewType == 2) {
        /* [200, 10, *] */
        f1.parentNode.style.display = "block";
        f1.parentNode.style.width = "250px";
        f2.style.marginLeft = "240px";
        f2.style.display = "block";
        f1.style.display = "block";
        f3.style.marginLeft = "250px";
        f4.src = "/resource/images/lt.gif";
    }
    else if(viewType == 3) {
        /* [0, 10, *] */
        f1.style.display = "none";
        f1.parentNode.style.display = "block";
        f1.parentNode.style.width = "10px";
        f2.style.marginLeft = "0px";
        f2.style.display = "block";
        f3.style.marginLeft = "10px";
        f4.src = "/resource/images/gt.gif";
    }
    else if(viewType == 4) {
        /* [0, 0, *] */
        f1.parentNode.style.display = "none";
        f3.style.marginLeft = "0px";
        f4.src = "/resource/images/gt.gif";
    }
};

App.nav = function(src) {
    var leftUrl  = src.getAttribute("leftUrl");
    var viewUrl  = src.getAttribute("viewUrl");
    var viewType = src.getAttribute("viewType");

    leftUrl  = (leftUrl != null ? decodeURIComponent(leftUrl) : "#");
    viewUrl  = (viewUrl != null ? decodeURIComponent(viewUrl) : "#");
    viewType = (viewType != null ? decodeURIComponent(viewType) : "#");

    if(viewType != "4" && leftUrl != null && leftUrl != "#") {
        App.setLeftUrl(leftUrl);
    }

    if(viewUrl != null && viewUrl != "#") {
        App.setViewUrl(viewUrl);
    }
    App.setViewType(viewType);
};

App.getContextPath = function() {
    if(this.contextPath == null || this.contextPath == undefined) {
        var contextPath = document.body.getAttribute("contextPath");

        if(contextPath == null || contextPath == "/") {
            contextPath = "";
        }
        this.contextPath = contextPath;
    }
    return this.contextPath;
};

App.setLeftUrl = function(url){
    document.getElementById("leftFrame").src = url;
};

App.setViewUrl = function(url){
    document.getElementById("mainFrame").src = url;
};

App.refresh = function() {
    window.location.reload();
};

App.help = function() {
};

App.logout = function() {
    var expires = new Date();
    expires.setTime(expires.getTime() - 60 * 60 * 1000);
    document.cookie = "passport=; expires=" + expires.toUTCString() + "; path=/; domain=.mytest.com;";
    window.location.reload();
};

jQuery(function() {
    App.resize = function(){
        var offset = document.getElementById("viewPanel").offsetTop + 24;
        var clientHeight = document.documentElement.clientHeight;
        document.getElementById("leftFrame").style.height = (clientHeight - offset) + "px";
        document.getElementById("mainFrame").style.height = (clientHeight - offset) + "px";
        document.getElementById("ctrlPanel").style.height = (clientHeight - offset) + "px";
    };

    jQuery("body").click(function() {
        jQuery("#setting_menu").hide();
        jQuery("#application_menu").hide();
    });

    jQuery("#ctrlPanel").hover(function(event){
        this.style.backgroundColor = "#aec9fb";
    }, function(event){
        this.style.backgroundColor = "#dfe8f6";
    });
    jQuery("#ctrlBtn").click(function(event){
        var viewType = 2;

        if(this.src.indexOf("/resource/images/gt.gif") > -1)
        {
            viewType = 2;
        }
        else
        {
            viewType = 3;
        }
        App.setViewType(viewType);
    });

    jQuery("#statusBar").show();
    jQuery(window).bind("resize", App.resize);
    App.resize();
});

jQuery(function() {
    jQuery("div.module-group-menu-bar ul li").hover(function() {
        jQuery(this).addClass("hover");
    }, function() {
        jQuery(this).removeClass("hover");
    });

    jQuery("div.module-group-menu-bar ul li").click(function(event) {
        if(jQuery(this).closest("div.contextmenu").size() > 0) {
            App.nav(this);
            jQuery(this).closest("div.contextmenu").closest("li").mouseout();
            return;
        }

        jQuery(this).siblings().removeClass("clicked").removeClass("hover");
        jQuery(this).addClass("clicked");

        var target = (event.target || event.srcElement);

        if(this != target) {
            return;
        }

        var leftUrl  = this.getAttribute("leftUrl");
        var viewUrl  = this.getAttribute("viewUrl");
        var viewType = this.getAttribute("viewType");

        leftUrl  = (leftUrl != null ? decodeURIComponent(leftUrl) : "#");
        viewUrl  = (viewUrl != null ? decodeURIComponent(viewUrl) : "#");
        viewType = (viewType != null ? decodeURIComponent(viewType) : "#");

        if(viewType != "4" && leftUrl != null && leftUrl != "#") {
            App.setLeftUrl(leftUrl);
        }

        if(viewUrl != null && viewUrl != "#") {
            App.setViewUrl(viewUrl);
        }
        App.setViewType(viewType);
    });
    jQuery("#module_group_menu li:first").click();
});

jQuery(function() {
    var showContextMenu = function() {
        var e = (event || window.event);
        var src = (e.srcElement || e.target);
        if(src.nodeName.toLowerCase() == "a") {
            // Finder.select(src.parentNode);
        }
        else {
            // Finder.select(src);
        }

        var panel = document.getElementById("contextmenu");
        panel.style.top = (document.body.scrollTop + event.clientY) + "px";
        panel.style.left = (document.body.scrollLeft + event.clientX) + "px";
        panel.style.display = "block";
        return false;
    };

    /*
    jQuery(document).bind("contextmenu", function(event){
        return showContextMenu(event);
    });
    */
});