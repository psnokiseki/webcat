delete from system_variable;
insert into system_variable(variable_name, variable_value, variable_desc) values ('name',                   '管理控制台', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('title',                  '管理控制台', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('home',                   'index.html', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('backgroundColor',        '#ffffff', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('enterpriseMail',         'abc@abc.com', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('logo',                   '/resource/images/logo.gif', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('banner',                 '/resource/images/banner.swf', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('webmasterName',          '110', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('webMasterTel',           '010-87654321', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('webMasterFax',           '010-87654321', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('webMasterMail',          'abc969@abc.com', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('ICPNO',                  '1202403', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('copyright',              '<em>All Rights Reserved</em> 版权所有', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('address',                'www.webcat.com', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('postCode',               '100083', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('status',                 '1', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('uploadFilePath',         'upload', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('uploadFileType',         'gif|jpg|bmp|png|swf|doc', '');

insert into system_variable(variable_name, variable_value, variable_desc) values ('maxUploadFileSize',      '2048', '最大上传文件大小单位KB');
insert into system_variable(variable_name, variable_value, variable_desc) values ('resizeImageEnabled',     'true', '是否允许缩放图片');
insert into system_variable(variable_name, variable_value, variable_desc) values ('maxImageWidth',          '800',  '缩放图片的最大宽度');
insert into system_variable(variable_name, variable_value, variable_desc) values ('maxImageHeight',         '600',  '缩放图片的最大高度');

insert into system_variable(variable_name, variable_value, variable_desc) values ('sessionTimeout',         '10', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('developer',              'www.skinsoft.com', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('developerWebSite',       'www.skinsoft.com', '');
insert into system_variable(variable_name, variable_value, variable_desc) values ('contextPath',            '/', '上下文路径');
insert into system_variable(variable_name, variable_value, variable_desc) values ('version',                '1.0.0', '版本号');

delete from skin_schedule;
insert into skin_schedule(schedule_id, schedule_name, description, schedule_type, status, owner, expression, action, properties, last_fire_time, next_fire_time, execute_status, execute_result, create_time, update_time) values (1, 'HelloJob', '定时输出Hello', 1, 0, 'anyone', '0 0/5 * * * ? *', 'com.skin.finder.schedule.HelloJob', '', '2016-12-29 22:30:00 000', '2016-12-29 22:31:00 000', 200, 'success', '2016-12-29 22:24:54 000', '2016-12-29 22:30:00 000');
insert into skin_schedule(schedule_id, schedule_name, description, schedule_type, status, owner, expression, action, properties, last_fire_time, next_fire_time, execute_status, execute_result, create_time, update_time) values (2, '备份数据', '定时备份数据', 1, 1, 'anyone', '0 10 0 * * ? *', 'com.skin.webcat.job.BackupJob', '', null, null, 0, '', '2016-12-29 22:30:11 000', '2016-12-29 22:30:11 000');
