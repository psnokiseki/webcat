drop table if exists skin_captcha;
create table skin_captcha(
    captcha_id      varchar(64)              not null,
    captcha_code    varchar(8)               not null,
    captcha_expiry  datetime                 not null,
    primary key (captcha_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists skin_user_session;
create table skin_user_session(
    session_id           bigint(20)          not null,
    app_id               bigint(20)          not null,
    user_id              bigint(20)          not null,
    user_name            varchar(32)         not null,
    nick_name            varchar(32)         not null,
    client_id            varchar(64)         not null,
    create_time          datetime            not null,
    last_access_time     datetime,
    primary key (session_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists skin_sequences;
create table skin_sequences(
    sequence_name  varchar(64)           not null,
    sequence_value bigint(20)            not null,
    primary key (sequence_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists skin_schedule;
create table skin_schedule(
    schedule_id      bigint(20) not null auto_increment,
    schedule_name    varchar(64) not null,
    description      varchar(128) not null,
    schedule_type    int(11) not null default 0,
    status           int(11) not null default 0,
    owner            varchar(64) not null,
    expression       varchar(64),
    action           varchar(255) not null,
    properties       text,
    last_fire_time   datetime,
    next_fire_time   datetime,
    execute_status   int(11) default 0,
    execute_result   varchar(128),
    create_time      datetime not null,
    update_time      datetime not null,
    primary key (schedule_id)
);

drop table if exists skin_schedule_log;
create table skin_schedule_log(
    log_id           bigint(20) not null auto_increment,
    schedule_id      bigint(20) not null,
    schedule_name    varchar(255) not null,
    schedule_type    int(11) not null default 0,
    invocation       varchar(64) not null default 'auto',
    fire_time        datetime,
    next_fire_time   datetime,
    execute_status   int(11) default 0,
    execute_result   varchar(128),
    primary key (log_id)
);

drop table if exists system_variable;
create table system_variable(
    variable_name                  varchar(64)         not null,
    variable_value                 varchar(255)        not null,
    variable_desc                  varchar(255),
    primary key (variable_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists skin_keyword;
create table skin_keyword(
    keyword_id           int(11)       not null auto_increment,
    keyword_type         int(11)       not null,
    keyword              varchar(64)   default null,
    create_time          datetime      not null default '00-00-00 00:00:00',
    primary key(keyword_id)
) ENGINE=InnoDB default charset=utf8 collate=utf8_unicode_ci;

drop table if exists skin_access_log;
create table skin_access_log(
    log_id                         int(11)             not null auto_increment,
    access_time                    datetime            not null,
    user_id                        int(11)             not null default 0,
    user_name                      varchar(32),
    local_ip                       varchar(32),
    thread_name                    varchar(64),
    remote_host                    varchar(100)        not null,
    request_method                 varchar(12)         not null,
    request_protocol               varchar(12)         not null,
    request_url                    varchar(2560)       not null,
    request_referer                varchar(2560)       not null,
    client_id                      varchar(64)         not null,
    client_user_Agent              varchar(255)        not null,
    client_cookie                  varchar(2560)       not null,
    primary key (log_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

